package ajlanawards.mivor.com.ajlanawards;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ApiClient.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class add_log extends AppCompatActivity {
    Button send;
    EditText title;
    EditText content;
    EditText phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_log);
        setTitle(getString(R.string.new_log));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
         title = (EditText) findViewById(R.id.title);
         content = (EditText) findViewById(R.id.comment);
         phone = (EditText) findViewById(R.id.phone);
        send = (Button) findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title.setError(null);
                content.setError(null);
                
                String _title =  title.getText().toString();
                String _content = content.getText().toString();
                if(TextUtils.isEmpty(_title)){
                    title.setError(getString(R.string.error_field_required));
                }else if(TextUtils.isEmpty(_content)){
                    content.setError(getString(R.string.error_field_required));

                }else{
                    String data  = "{\n" +
                            "  \"title\": \""+_title+"\", \n" +
                            "  \"type\": \"testimonial\",\n" +
                            "  \"field_comment_phone\":  {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+phone.getText().toString()+"\"\n" +
                            "      }\n" +
                            "    ]},\n" +
                            "  \"field_testimonial_comment\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+_content+"\"\n" +
                            "      }\n" +
                            "    ]}\n" +
                            "}";

                    InsertLog(data);
                }
            }
        });

    }

    private void InsertLog(String data) {
        Call<String> call = ApiClient.getApiInterface().InsertLog(data,"application/json");
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.body()!=null){
                    if(response.body().contains("nid")){
                        Toast.makeText(add_log.this, R.string.done_success, Toast.LENGTH_SHORT).show();
                        setResult(20);
                        finish();

                    }

                }else{
                    Toast.makeText(add_log.this,getString(R.string.try_again), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(add_log.this, getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
