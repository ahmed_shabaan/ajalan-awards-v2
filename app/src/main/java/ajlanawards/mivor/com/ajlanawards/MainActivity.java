package ajlanawards.mivor.com.ajlanawards;

import android.app.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;

import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.MenuInflater;
import android.view.SubMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;

import Accessors.CustomTextView;
import Accessors.CustomTypefaceSpan;
import Accessors.GlobalKeys;
import Accessors.PrefManager;
import fragments.GallaryList;
import fragments.Homefragment;
import fragments.about_awards;
import fragments.about_us;
import fragments.candidates;
import fragments.contact_us;
import fragments.gallary;
import fragments.news;
import fragments.prizeComunity;
import fragments.prizeinfoList;
import fragments.profile;
import fragments.registration_history;
import fragments.rounds;
import fragments.vedioGallary;
import fragments.visitors_senders;

public class MainActivity extends AppCompatActivity
implements NavigationView.OnNavigationItemSelectedListener , gallary.OnFragmentInteractionListener
        ,GallaryList.OnFragmentInteractionListener,
        Homefragment.OnFragmentInteractionListener ,candidates.OnFragmentInteractionListener
        ,prizeinfoList.OnFragmentInteractionListener , vedioGallary.OnFragmentInteractionListener, prizeComunity.OnFragmentInteractionListener{


    FragmentManager manager;
    FragmentTransaction transaction;

    Fragment list;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

         setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        prefManager = new PrefManager(this);
        GlobalKeys.setLocale(prefManager.get_lang(),this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        Homefragment homefragment = new Homefragment();
        transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        transaction.replace(R.id.myfrgmentlayout, homefragment, "Homefragment");
        transaction.addToBackStack("Home");
        transaction.commit();



        Menu nav_Menu = navigationView.getMenu();
        CustomTextView  register = (CustomTextView) navigationView.getHeaderView(0).findViewById(R.id.register);
        CustomTextView  login = (CustomTextView) navigationView.getHeaderView(0).findViewById(R.id.login);


        if(prefManager.check_login()){
            register.setVisibility(View.GONE);
            login.setVisibility(View.GONE);

            nav_Menu.findItem(R.id.edit_profile).setVisible(true);
            nav_Menu.findItem(R.id.logout).setVisible(true);
            nav_Menu.findItem(R.id.history).setVisible(true);

        }else {

            register.setVisibility(View.VISIBLE);
            login.setVisibility(View.VISIBLE);
            nav_Menu.findItem(R.id.edit_profile).setVisible(false);
            nav_Menu.findItem(R.id.logout).setVisible(false);
            nav_Menu.findItem(R.id.history).setVisible(false);

        }

        for (int i=0;i<nav_Menu.size();i++) {
            MenuItem mi = nav_Menu.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,SingUp.class);
                startActivityForResult(intent, GlobalKeys.LOGIN_AND_REGISTER_REQUEST);


        }});
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,Login.class);
                startActivityForResult(intent, GlobalKeys.LOGIN_AND_REGISTER_REQUEST);


            }});





    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/font.TTF");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }


        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {

        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            finish();

        }
    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.candidates) {

            transaction = manager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

            candidates candidates = new candidates();

            //   menuItemOne.setArguments(b);
            transaction.replace(R.id.myfrgmentlayout,candidates , "ItemOnea");
            transaction.addToBackStack("Homea");
            transaction.commit();

            // Handle the camera action
        }else if(id == R.id.home){
            transaction = manager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

            Homefragment homefragment = new Homefragment();
            transaction.replace(R.id.myfrgmentlayout, homefragment, "Homefragment");
            transaction.addToBackStack("Home");
            transaction.commit();
        } else if(id == R.id.history){
            transaction = manager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
            registration_history registration_history = new registration_history();
            transaction.replace(R.id.myfrgmentlayout, registration_history, "registration_history");
            transaction.addToBackStack("registration_history");
            transaction.commit();
        } else if (id == R.id.gallery) {

            transaction = manager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

            GallaryList gallary = new GallaryList();
            //   menuItemOne.setArguments(b);
            transaction.replace(R.id.myfrgmentlayout,gallary , "ItemOne");
            transaction.addToBackStack("Home");
            transaction.commit();


        } else if (id == R.id.prizenews) {
            transaction = manager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

            news news = new news();
            //   menuItemOne.setArguments(b);

            transaction.replace(R.id.myfrgmentlayout,news , "news");
            transaction.addToBackStack("news");
            transaction.commit();


        }else if (id == R.id.edit_profile) {

            transaction = manager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

            profile profile = new profile();
            //   menuItemOne.setArguments(b);

            transaction.replace(R.id.myfrgmentlayout,profile , "profile");
            transaction.addToBackStack("profile");
            transaction.commit();
        }
        else if (id == R.id.contactUs) {
            transaction = manager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

            visitors_senders visitors_senders = new visitors_senders();
            transaction.replace(R.id.myfrgmentlayout,visitors_senders , "visitors_senders");
            transaction.addToBackStack("visitors_senders");
            transaction.commit();

        }else if (id == R.id.aboutus) {
            transaction = manager.beginTransaction();
            about_us about_us = new about_us();
            transaction.replace(R.id.myfrgmentlayout,about_us , "about_us");
            transaction.addToBackStack("about_us");
            transaction.commit();

        }else if (id == R.id.winners) {
            transaction = manager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

            rounds rounds = new rounds();
            transaction.replace(R.id.myfrgmentlayout,rounds , "rounds");
            transaction.addToBackStack("rounds");
            transaction.commit();

        }else if (id == R.id.logout) {
            PrefManager prefManager = new PrefManager(this);
            prefManager.logout();
            Intent i = getIntent();
            finish();
            startActivity(i);

        }else if (id == R.id.login) {
            Intent intent = new Intent(this,Login.class);
            startActivityForResult(intent, GlobalKeys.LOGIN_AND_REGISTER_REQUEST);

        }else if (id == R.id.register) {
            Intent intent = new Intent(this,SingUp.class);
            startActivityForResult(intent, GlobalKeys.LOGIN_AND_REGISTER_REQUEST);

        }else if(id == R.id.about_award){
            transaction = manager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
            about_awards about_awards = new about_awards();
            transaction.replace(R.id.myfrgmentlayout,about_awards , "about_awards");
            transaction.addToBackStack("about_awards");
            transaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==GlobalKeys.LOGIN_AND_REGISTER_REQUEST && resultCode==GlobalKeys.LOGIN_AND_REGISTER_REQUEST){
            Intent i = getIntent();
            finish();
            startActivity(i);

        }else{
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            GlobalKeys.change_language(MainActivity.this);
            return true;
        }



        return false;
    }
}
