package ajlanawards.mivor.com.ajlanawards;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.Locale;

import Accessors.GlobalKeys;
import Accessors.PrefManager;

public class splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.splash);
        PrefManager prefManager = new PrefManager(this);

        GlobalKeys.setLocale(prefManager.get_lang(),this);
        GlobalKeys.lang = prefManager.get_lang();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // Intent intent = new Intent(splash.this, Login.class);
                 startActivity((new Intent(splash.this,MainActivity.class)));
                 finish();
           }
      }, 1000);
    }
}
