package ajlanawards.mivor.com.ajlanawards;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;

/**
 * Created by ahmed on 04/12/16.
 */

public class VedioPlayer extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vedioplayer_layout);
        String URl  = getIntent().getStringExtra("url");
        WebView mWebView = (WebView) findViewById(R.id.vplayer);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.loadUrl(URl);
        mWebView.setWebChromeClient(new WebChromeClient());

    }
}
