package ajlanawards.mivor.com.ajlanawards;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import Accessors.CustomEditText;
import Accessors.CustomTextView;
import Accessors.GlobalKeys;
import Accessors.PrefManager;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class Login extends AppCompatActivity  {

    String mUserName  , mPassword ;
    CustomEditText et_UserName , et_Password ;
    Button btn_signIn;

    String  Result;
    CustomTextView singuptxt,forgettxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.login);
        et_UserName =(CustomEditText) findViewById(R.id.email);
        et_Password =(CustomEditText) findViewById(R.id.etPassword);
        btn_signIn = (Button) findViewById(R.id.signInButton);
        btn_signIn.setTypeface(GlobalKeys.getFont(this));


        singuptxt = (CustomTextView) findViewById(R.id.singuptxt);
        forgettxt = (CustomTextView) findViewById(R.id.forget_password);

        singuptxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Login.this, SingUp.class);
                startActivityForResult(intent, GlobalKeys.LOGIN_AND_REGISTER_REQUEST);


            }
        });

        forgettxt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, forget_password.class);
                startActivity(intent);
            }
        });
        btn_signIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                et_UserName.setError(null);
                et_Password.setError(null);
               if( et_UserName.getText().toString().isEmpty())
               {
                   et_UserName.setError(getString(R.string.error_field_required));
               }
                else if (et_Password.getText().toString().isEmpty())
               {
                    et_Password.setError(getString(R.string.error_field_required));
               }
                else
                {

                     mPassword = et_Password.getText().toString();
                    mUserName = et_UserName.getText().toString();

                     new HttpRequestTask().execute();
                }

            }
        });

    }



    private class HttpRequestTask extends AsyncTask<Void, Void ,Void> {
        public ProgressDialog progress;
        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(Login.this);
            progress.setMessage(getString(R.string.loading));
            progress.setCancelable(false);
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {



            try {

                String InsertTransactionResult = null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpParams myParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(myParams, 1000);
                HttpConnectionParams.setSoTimeout(myParams, 1000);

                try {

                    HttpPost httppost = new HttpPost(GlobalKeys.LOGINARTINURL);
                    httppost.setHeader("Content-type", "application/json");


//                    StringEntity se = new StringEntity("\"{\" +\n" +
//                            "                                \"  \\\"username\\\":\""+ mUserName + "\",\" +\n" +
//                            "                                \"\\\"password\\\":\""+ mPassword + "\"}\"");7
                    StringEntity se = new StringEntity(" {\n" +
                            "                        \"username\": \""+mUserName+"\",\n" +
                            "                            \"password\":\""+mPassword+"\"\n" +
                            "                    }");

                    se.setContentEncoding(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE,
                            "application/json"));
                    httppost.setEntity(se);

                    HttpResponse response = httpclient.execute(httppost);

                    Result = EntityUtils
                            .toString(response.getEntity());

                    Log.d("Response" , ""+Result);

                } catch (ClientProtocolException e) {

                } catch (IOException e) {
                }

            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;



        }

        @Override
        protected void onPostExecute(Void aVoid) {

            progress.dismiss();

            if(Result.contains("sessid"))
            {
                long  user_id = 0;
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(Result);
                    jsonObject = jsonObject.getJSONObject("user");
                    user_id = jsonObject.getLong("uid");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                setResult(GlobalKeys.LOGIN_AND_REGISTER_REQUEST);
                finish();
                PrefManager prefManager = new PrefManager(Login.this);
                prefManager.setLogin();
                prefManager.saveUserID(user_id);
                prefManager.set_username_password(mUserName,mPassword);

            }else {
                /*Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);*/
              Toast.makeText(Login.this, R.string.login_error, Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==GlobalKeys.LOGIN_AND_REGISTER_REQUEST && resultCode==GlobalKeys.LOGIN_AND_REGISTER_REQUEST){
            setResult(requestCode);
            finish();
        }
    }
}

