package ajlanawards.mivor.com.ajlanawards;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import Accessors.GlobalKeys;
import models.article;
import models.news;

public class single_articles_activity extends AppCompatActivity {
    private String imageTransitionName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_articles_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(null);

        Bundle b = getIntent().getExtras();
        article item  = new article();
        item = item.FromBundle(b);
        imageTransitionName = getIntent().getStringExtra("myname");
        TextView title =  findViewById(R.id.title);
        TextView body = findViewById(R.id.description);
        ImageView cover = (ImageView) findViewById(R.id.cover);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cover.setTransitionName(imageTransitionName);
        }

        title.setText(item.getTitle());
        body.setText(Html.fromHtml(item.getBody()).toString());

        String img= GlobalKeys.IMAGE_URL+item.getImage();
        Picasso.with(this)
                .load(img)
                .into(cover, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
