package ajlanawards.mivor.com.ajlanawards;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Region;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import Accessors.GlobalKeys;
import Accessors.LoginRequest;
import Accessors.LoginResponse;
import Accessors.PrefManager;
import Accessors.utils;

import static java.net.Proxy.Type.HTTP;

public class SingUp extends AppCompatActivity {


    EditText et_email ,et_fname,et_middlname,et_lastname ,et_phoneNumber,et_loginName,forth_name;
    EditText et_Password;
    EditText et_confirmPassword;
    Button  btn_signInButton ;
    String mEmail , mPassword , mFirstName ,mMiddleName, mLastName , mMobileNumber , mLoginName,fourth_name ;
    String  Result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

         et_email = (EditText) findViewById(R.id.email);
        et_fname = (EditText) findViewById(R.id.fname);
        et_lastname= (EditText) findViewById(R.id.lastname);
        et_middlname = (EditText) findViewById(R.id.middlename);
        et_loginName= (EditText) findViewById(R.id.loginName);
        et_phoneNumber= (EditText) findViewById(R.id.mobilenumer);
        forth_name= (EditText) findViewById(R.id.forth_name);

         et_Password = (EditText) findViewById(R.id.etPassword);
         et_confirmPassword = (EditText) findViewById(R.id.confirmPassword);
         btn_signInButton = (Button) findViewById(R.id.signInButton);
        btn_signInButton.setTypeface(GlobalKeys.getFont(this));
        btn_signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(et_fname.getText().toString().isEmpty())
                {
                   et_fname.setError(getString(R.string.error_field_required));
                }
                else  if (et_middlname.getText().toString().isEmpty())
                {
                    et_middlname.setError(getString(R.string.error_field_required));
                }

                else   if(et_lastname.getText().toString().isEmpty())
                {
                    et_lastname.setError(getString(R.string.error_field_required));
                }



                else  if (et_loginName.getText().toString().isEmpty())
                {
                    et_loginName.setError(getString(R.string.error_field_required));
                }



                else if(et_email.getText().toString().isEmpty())
               {
                   et_email.setError(getString(R.string.error_field_required));
               }
                else if(!utils.isEmailValid(et_email.getText().toString()))
                {
                    et_email.setError(getString(R.string.error_invalid_email));
                }
                else if(et_Password.getText().toString().isEmpty())
                {
                    et_Password.setError(getString(R.string.error_field_required));
                }
                else if((et_confirmPassword.getText().toString().isEmpty()))
                {
                    et_confirmPassword.setError(getString(R.string.error_field_required));
                }
                else if(!(et_Password.getText().toString().equals(et_confirmPassword.getText().toString())))
                {
                    et_Password.setError(getString(R.string.password_not_matched));
                }


                else  if (et_phoneNumber.getText().toString().isEmpty())
                {
                    et_phoneNumber.setError(getString(R.string.error_field_required));
                }
                else {
                    mPassword =  et_Password.getText().toString();
                    mEmail   = et_email.getText().toString();
                    mFirstName   = et_fname.getText().toString();
                    mLastName  = et_lastname.getText().toString();
                    mMiddleName   = et_middlname.getText().toString();
                    mMobileNumber  = et_phoneNumber.getText().toString();
                    mLoginName = et_loginName.getText().toString();
                   new HttpRequestTask().execute();
               }

            }
        });







    }



    private class HttpRequestTask extends AsyncTask<Void, Void ,Void> {

        public ProgressDialog progress;
        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(SingUp.this);
            progress.setMessage(getString(R.string.loading));
            progress.setCancelable(false);
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                String InsertTransactionResult = null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpParams myParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(myParams, 1000);
                HttpConnectionParams.setSoTimeout(myParams, 1000);

                try {
                    fourth_name  = "_none";
                    HttpPost httppost = new HttpPost(GlobalKeys.REGESTARTINURL);
                    httppost.setHeader("Content-type", "application/json");
                    StringEntity se = new StringEntity("{\n" +
                            "   \"name\":\""+mLoginName+"\",\n" +
                            "   \"pass\":\""+mPassword+"\",\n" +
                            "   \"mail\":\""+mEmail+"\",\n" +
                            "   \"field_first_name\":{\n" +
                            "      \"und\":[\n" +
                            "         {\n" +
                            "            \"value\":\""+mFirstName+"\"\n" +
                            "         }\n" +
                            "      ]\n" +
                            "   },\n" +
                            "   \"field_middle_name\":{\n" +
                            "      \"und\":[\n" +
                            "         {\n" +
                            "            \"value\":\""+mMiddleName+"\"\n" +
                            "         }\n" +
                            "      ]\n" +
                            "   },\n" +
                            "   \"field_fourth_name\": {" +
                             "    \"und\": [\n" +
                                "      {\n" +
                                "        \"value\": \""+fourth_name+"\"\n" +
                                "      }\n" +
                                "    ]\n" +
                                "  },\n" +
                            "   \"field_last_name\":{\n" +
                            "      \"und\":[\n" +
                            "         {\n" +
                            "            \"value\":\""+mLastName+"\"\n" +
                            "         }\n" +
                            "      ]\n" +
                            "   },\n" +
                            "   \"field_mobile_number\":{\n" +
                            "      \"und\":[\n" +
                            "         {\n" +
                            "            \"value\":\""+mMobileNumber+"\"\n" +
                            "         }\n" +
                            "      ]\n" +
                            "   }\n" +
                            "}");
                    se.setContentEncoding(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE,
                            "application/json"));
                    httppost.setEntity(se);

                    HttpResponse response = httpclient.execute(httppost);

                     Result = EntityUtils
                            .toString(response.getEntity());

                    Log.d("Response" , ""+Result);

                } catch (ClientProtocolException e) {

                } catch (IOException e) {
                }

            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            progress.dismiss();
            if(Result != null){
                if (Result.contains("form_errors"))
                {
                    String ErrorMsg="";
                    JSONObject myJson = null;
                    try {
                        myJson = new JSONObject(Result);
                        myJson = myJson.getJSONObject("form_errors");
                        //String name = myJson.getString("name");

                        if(myJson.has("name")){
                            ErrorMsg+= myJson.getString("name");
                        }
                        if(myJson.has("mail")){
                            ErrorMsg+=  myJson.getString("mail");
                        }


                        Toast.makeText(SingUp.this,ErrorMsg ,Toast.LENGTH_LONG ).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // use myJson as needed, for example

                }
                else {
                    long  user_id = 0;
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(Result);
                        user_id = jsonObject.getLong("uid");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    setResult(GlobalKeys.LOGIN_AND_REGISTER_REQUEST);
                    finish();
                    PrefManager prefManager = new PrefManager(SingUp.this);
                    prefManager.setLogin();
                    prefManager.saveUserID(user_id);
                    prefManager.set_username_password(et_loginName.getText().toString().trim(), mPassword);


                }
            }else{
                Toast.makeText(SingUp.this, getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
            }


            super.onPostExecute(aVoid);
        }
    }

}
