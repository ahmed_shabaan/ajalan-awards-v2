package ajlanawards.mivor.com.ajlanawards;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.List;

import Accessors.GlobalKeys;
import ApiClient.ApiClient;
import adapters.NewsAdaptor;
import models.news;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class forget_password extends AppCompatActivity {
    ProgressBar progressBar;
    Button reset;
    EditText nameOrEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        getSupportActionBar().hide();
        progressBar = (ProgressBar) findViewById(R.id.ProgressBar);
        reset = (Button) findViewById(R.id.reset);
        reset.setTypeface(GlobalKeys.getFont(this));
        nameOrEmail = (EditText) findViewById(R.id.nameOremail);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameOrEmail.setError(null);
                String _nameORemail = nameOrEmail.getText().toString();
                if(TextUtils.isEmpty(_nameORemail)){
                    nameOrEmail.setError(getString(R.string.error_field_required));
                }else{
                    String body = "{\n" +
                            "\t\"name\":\""+_nameORemail+"\"\n" +
                            "}";
                    forget_password(body) ;
                }
            }
        });
    }

    private void forget_password(String body) {
        progressBar.setVisibility(View.VISIBLE);
        final Call<String> forgetPassword = ApiClient.getApiInterface().ForgetPassword("application/json",body);

        forgetPassword.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressBar.setVisibility(View.GONE);
                String results  = response.body();
                if(results !=null && results.contains("true")){
                    Toast.makeText(forget_password.this,"تم ارسال كود الى الاميل", Toast.LENGTH_SHORT).show();
                    finish();
                }else{
                    Toast.makeText(forget_password.this,getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                }



            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(forget_password.this, getString(R.string.internet_error), Toast.LENGTH_SHORT).show();


            }
        });
    }
}
