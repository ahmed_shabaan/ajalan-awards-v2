package ApiClient;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import Accessors.GlobalKeys;
import models.RegistrationHistoryResponse;
import models.active_awards;
import models.article;
import models.award;
import models.award_version;
import models.log;
import models.news;
import models.round;
import models.video;
import models.winner;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by ahmed shabaan on 12/15/2016.
 */
public interface ApiInterface {
    @GET(GlobalKeys.VIDEOURL)
    Call<List<video>> getVideos(@Path("path") String lang,@Query("args[0]") long id);




    @GET(GlobalKeys.NEWSURL)
    Call<List<news>> getNews(@Path("path") String lang,@Query("args[0]") long id);
    @GET(GlobalKeys.AROUNDURL)
    Call<ArrayList<round>> getRounds(@Path("path") String lang);

    @GET(GlobalKeys.AWARDS)
    Call<List<award>> getAwards(@Path("path") String lang,@Query("args[0]") long id);
    @GET(GlobalKeys.ARTICLES)
    Call<List<article>> getArticles(@Path("path") String lang,@Query("args[0]") long id);

    @GET(GlobalKeys.LOGS)
    Call<List<log>> getLogs();

    @GET(GlobalKeys.VERSIONS)
    Call<List<award_version>> getVersions(@Path("path") String lang);
    @GET(GlobalKeys.WINNERS)
    Call<List<winner>> getWinners(@Path("path") String lang,@Query("args[0]") long id);

    @POST(GlobalKeys.FORGETPASSWORD)
    Call<String> ForgetPassword(@Header("Content-Type") String content_type,@Body String body);

    @POST("test/file.json")
    Call<String> uploadFileToServer(
            @Body String content);


    @POST("test/node.json")
    Call<String> InsertLog(@Body  String data,@Header("Content-Type") String content_json);


    @GET("test/views/awards_api.json")
    Call<List<active_awards>> getActiveAwards();


    @GET("{path}/test/views/my_registers.json")
    Call<ArrayList<RegistrationHistoryResponse>> gethistory(@Path("path") String lang,@Header("Content-Type") String content_json,@Header("Authorization") String Authorization);

    @GET("{path}/test/node/{path1}.json")
    Call<String> getSingleAward(@Path("path") String lang,@Path("path1") long id,@Header("Content-Type") String content_json,@Header("Authorization") String Authorization);
}
