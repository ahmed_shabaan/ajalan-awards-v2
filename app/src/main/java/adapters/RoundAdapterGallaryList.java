package adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import Accessors.GlobalKeys;
import Accessors.RoundClickCallBack;
import ajlanawards.mivor.com.ajlanawards.R;
import fragments.articles;
import fragments.gallary;
import fragments.news;
import fragments.single_article;
import fragments.vedioGallary;
import models.article;
import models.round;

/**
 * Created by ahmed shabaan on 3/28/2017.
 */

public class RoundAdapterGallaryList  extends RecyclerView.Adapter<RoundAdapterGallaryList.ViewHolder> {

    private static List<round> dataSet;
    public Context context;
    static RoundClickCallBack roundClickCallBack;



    public RoundAdapterGallaryList(List<round> os_versions, Context context, RoundClickCallBack roundClickCallBack) {
        dataSet = os_versions;
        this.context = context;
        this.roundClickCallBack = roundClickCallBack;
    }


    @Override
    public RoundAdapterGallaryList.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
// create a new view
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.round_item, null);

        // create ViewHolder

        RoundAdapterGallaryList.ViewHolder viewHolder = new RoundAdapterGallaryList.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RoundAdapterGallaryList.ViewHolder viewHolder, final int i) {

        round fp = dataSet.get(i);

        viewHolder.title.setText(fp.getTitle());
        viewHolder.r = fp;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        round r;
        int type ;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            title = (TextView) itemLayoutView
                    .findViewById(R.id.title);

            itemLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    roundClickCallBack.itemClick(r.getId());
                }

            });




        }

    }
}



