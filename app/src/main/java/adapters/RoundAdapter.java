package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import Accessors.CustomTextView;
import ajlanawards.mivor.com.ajlanawards.R;
import models.round;

/**
 * Created by ahmed shabaan on 12/20/2016.
 */

public class RoundAdapter extends ArrayAdapter<round> {

    private final Context context;
    private List<round> rounds;


    public RoundAdapter(Context context, List<round> rounds) {
        super(context, -1, rounds);
        this.context = context;
        this.rounds = rounds;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.round_item, parent, false);
        TextView title = (TextView) rowView.findViewById(R.id.title);
        title.setText(rounds.get(position).getTitle());


        return rowView;
    }
}

