package adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import ajlanawards.mivor.com.ajlanawards.R;
import models.log;
import models.news;

/**
 * Created by ahmed shabaan on 1/3/2017.
 */
public class LogAdapter  extends RecyclerView.Adapter<LogAdapter.ViewHolder> {

    private static List<log> dataSet;
    public Context context;
    public Fragment f;


    public LogAdapter(List<log> os_versions, Context context,Fragment f) {

            dataSet = os_versions;
            this.context = context;
            this.f=f;
            }


    @Override
    public LogAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
    // create a new view
            View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
            R.layout.log_item, null);

            // create ViewHolder

            ViewHolder viewHolder = new ViewHolder(itemLayoutView);
            return viewHolder;
            }

    @Override
    public void onBindViewHolder(final LogAdapter.ViewHolder viewHolder, final int i) {

            log fp = dataSet.get(i);

            viewHolder.name.setText(fp.getName());
            viewHolder.comment.setText(fp.getComment());


        viewHolder.feed = fp;
    }

    @Override
    public int getItemCount() {
            return dataSet.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView comment;
        public ImageView iconView;
        public TextView description;
        public log feed;
        public ProgressBar loader;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            name = (TextView) itemLayoutView
                    .findViewById(R.id.name);
            comment = (TextView) itemLayoutView
                    .findViewById(R.id.comment);





        }

    }
}


