package adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import Accessors.GlobalKeys;
import ajlanawards.mivor.com.ajlanawards.R;
import ajlanawards.mivor.com.ajlanawards.single_articles_activity;
import ajlanawards.mivor.com.ajlanawards.single_news_activity;
import fragments.single_article;
import fragments.single_news;
import models.article;
import models.news;

/**
 * Created by ahmed shabaan on 1/2/2017.
 */
public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ViewHolder> {

    private static List<article> dataSet;
    public Context context;
    public Fragment f;


    public ArticlesAdapter(List<article> os_versions, Context context,Fragment f) {

        dataSet = os_versions;
        this.context = context;
        this.f=f;
    }


    @Override
    public ArticlesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
// create a new view
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.news_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ArticlesAdapter.ViewHolder viewHolder, final int i) {

        article fp = dataSet.get(i);

        viewHolder.title.setText(fp.getTitle());
        viewHolder.iconView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                article n = dataSet.get(i);
                Intent intent = new Intent(context, single_articles_activity.class);
                Bundle b ;
                b = n.ToBundle(n);
                b.putString("myname", ViewCompat.getTransitionName(viewHolder.iconView));
                intent.putExtras(b);

                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        (FragmentActivity) context,
                        viewHolder.iconView,
                        ViewCompat.getTransitionName(viewHolder.iconView));
                ((FragmentActivity) context).startActivity(intent, options.toBundle());
            }
        });

        String img= GlobalKeys.IMAGE_URL+fp.getImage();
        Picasso.with(context)
                .load(img)
                .into(viewHolder.iconView, new Callback() {
                    @Override
                    public void onSuccess() {
                        viewHolder.loader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        viewHolder.loader.setVisibility(View.GONE);
                    }
                });


        viewHolder.feed = fp;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public ImageView iconView;
        public article feed;
        public ProgressBar loader;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            title = (TextView) itemLayoutView
                    .findViewById(R.id.title);
            iconView = (ImageView) itemLayoutView
                    .findViewById(R.id.cover);

            loader = (ProgressBar) itemLayoutView.findViewById(R.id.loader);

        /*    iconView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // v.getContext().startActivity(new Intent(v.getContext(), Video_show.class).putExtra("id",feed.getUrl()));
                }

            });*/




        }

    }
}



