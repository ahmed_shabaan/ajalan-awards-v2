package adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import ajlanawards.mivor.com.ajlanawards.R;
import ajlanawards.mivor.com.ajlanawards.Video_show;
import models.award;
import models.video;

/**
 * Created by ahmed shabaan on 12/27/2016.
 */
public class Award_adapter extends RecyclerView.Adapter<Award_adapter.ViewHolder> {

    private static List<award> dataSet;
    public Context context;


    public Award_adapter(List<award> os_versions, Context context) {

        dataSet = os_versions;
        this.context = context;
    }


    @Override
    public Award_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
// create a new view
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.award_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final Award_adapter.ViewHolder viewHolder, int i) {

        award fp = dataSet.get(i);

        viewHolder.title.setText(fp.getNode_title());
        viewHolder.type.setText(fp.getAward_type());
        viewHolder.round.setText(fp.getAward_round());
        viewHolder.date.setText(fp.getCreated_date());
        viewHolder.status.setText(fp.getStatus());

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title,type,date,round,status;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            title = (TextView) itemLayoutView
                    .findViewById(R.id.title);
            type = (TextView) itemLayoutView
                    .findViewById(R.id.type);
            round = (TextView) itemLayoutView
                    .findViewById(R.id.round);
            date = (TextView) itemLayoutView
                    .findViewById(R.id.date);
            status = (TextView) itemLayoutView
                    .findViewById(R.id.status);





        }

    }
}


