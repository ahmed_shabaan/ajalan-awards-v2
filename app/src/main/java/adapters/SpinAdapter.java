package adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import Accessors.CustomTextView;
import models.SpinnerModel;

/**
 * Created by ahmed shabaan on 12/25/2016.
 */
public class SpinAdapter  extends ArrayAdapter<SpinnerModel> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<SpinnerModel> spinnerModels;

    public SpinAdapter(Context context, int textViewResourceId,
                       List<SpinnerModel> spinnerModels) {
        super(context, textViewResourceId, spinnerModels);
        this.context = context;
        this.spinnerModels = spinnerModels;
    }

    public int getCount(){
        return spinnerModels.size();
    }

    public SpinnerModel getItem(int position){
        return spinnerModels.get(position);
    }

    public long getItemId(int position){
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        CustomTextView label = new CustomTextView(context);
        label.setTextColor(Color.BLACK);

        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(spinnerModels.get(position).getValue());
        label.setPadding(3,3,3,3);


        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        CustomTextView label = new CustomTextView(context);
        label.setTextColor(Color.BLACK);
        label.setText(spinnerModels.get(position).getValue());
        label.setPadding(3,3,3,3);

        return label;
    }
}
