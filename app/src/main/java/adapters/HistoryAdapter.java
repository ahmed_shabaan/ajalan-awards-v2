package adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import Accessors.GlobalKeys;
import Accessors.PrefManager;
import ajlanawards.mivor.com.ajlanawards.R;
import ajlanawards.mivor.com.ajlanawards.single_articles_activity;
import fragments.award_registration;
import models.RegistrationHistoryResponse;
import models.article;

/**
 * Created by Ahmed shaban on 1/6/2018.
 */

public class HistoryAdapter  extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private static List<RegistrationHistoryResponse> dataSet;
    public Context context;
    public Fragment f;
    FragmentManager  fragmentManager;
    FragmentTransaction transaction;


    public HistoryAdapter(List<RegistrationHistoryResponse> os_versions, Context context) {

        dataSet = os_versions;
        this.context = context;
        this.f=f;
        fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
        transaction  = fragmentManager.beginTransaction();
    }


    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
// create a new view
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.history_item, null);

        // create ViewHolder

        HistoryAdapter.ViewHolder viewHolder = new HistoryAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final HistoryAdapter.ViewHolder viewHolder, final int i) {

        final RegistrationHistoryResponse fp = dataSet.get(i);

        viewHolder.title.setText(android.text.Html.fromHtml(fp.award).toString()
                );
        viewHolder.creation_date.setText(context.getString(R.string.creation_date)+fp.post_date);
        viewHolder.upadated_date.setText(context.getString(R.string.updated_date)+fp.updated_date);

        viewHolder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                award_registration award_registration = new award_registration();
                Bundle b = new Bundle();
                b.putInt("index", fp.nid);
                award_registration.setArguments(b);
                transaction.replace(R.id.myfrgmentlayout, award_registration, "award_registration");
                transaction.addToBackStack("award_registration");
                transaction.commit();
            }
        });



    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title,creation_date,upadated_date;
        Button edit;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            title =  itemLayoutView
                    .findViewById(R.id.title);

            creation_date =  itemLayoutView
                    .findViewById(R.id.creation_date);

            upadated_date =  itemLayoutView
                    .findViewById(R.id.updated_date);
            edit = itemLayoutView.findViewById(R.id.edit);


        /*    iconView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // v.getContext().startActivity(new Intent(v.getContext(), Video_show.class).putExtra("id",feed.getUrl()));
                }

            });*/




        }

    }
}