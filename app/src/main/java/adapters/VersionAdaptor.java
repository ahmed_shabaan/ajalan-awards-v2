package adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import Accessors.GlobalKeys;
import ajlanawards.mivor.com.ajlanawards.R;
import fragments.single_news;
import models.award_version;
import models.news;

/**
 * Created by ahmed shabaan on 1/3/2017.
 */
public class VersionAdaptor extends  RecyclerView.Adapter<VersionAdaptor.ViewHolder> {

    private static List<award_version> dataSet;
    public Context context;
    public Fragment f;


    public VersionAdaptor(List<award_version> os_versions, Context context,Fragment f) {

        dataSet = os_versions;
        this.context = context;
        this.f=f;
    }


    @Override
    public VersionAdaptor.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
// create a new view
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.version_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final VersionAdaptor.ViewHolder viewHolder, final int i) {

        award_version fp = dataSet.get(i);

        viewHolder.name.setText(fp.getTitle());
        viewHolder.date.setText(fp.getCreated_date());


        String img= GlobalKeys.GLBURL+fp.getImage();
        Picasso.with(context)
                .load(img)
                .into(viewHolder.iconView, new Callback() {
                    @Override
                    public void onSuccess() {
                        viewHolder.loader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        viewHolder.loader.setVisibility(View.GONE);
                    }
                });


        viewHolder.feed = fp;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name,date;
        public ImageView iconView,download;
        public award_version feed;
        public ProgressBar loader;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            name = (TextView) itemLayoutView
                    .findViewById(R.id.name);
            date = (TextView) itemLayoutView
                    .findViewById(R.id.date);
            download = (ImageView) itemLayoutView
                    .findViewById(R.id.download);
            iconView = (ImageView) itemLayoutView
                    .findViewById(R.id.cover);

            loader = (ProgressBar) itemLayoutView.findViewById(R.id.loader);

            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String pdf_url = GlobalKeys.GLBURL+feed.getPdf();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pdf_url));
                     v.getContext().startActivity(browserIntent);
                 }

            });




        }

    }
}


