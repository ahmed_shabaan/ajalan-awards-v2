package adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import Accessors.GlobalKeys;
import ajlanawards.mivor.com.ajlanawards.R;
import fragments.single_news;
import models.news;
import models.winner;

/**
 * Created by ahmed shabaan on 1/3/2017.
 */
public class WinnerAdapter extends RecyclerView.Adapter<WinnerAdapter.ViewHolder> {

    private static List<winner> dataSet;
    public Context context;
    public Fragment f;


    public WinnerAdapter(List<winner> os_versions, Context context,Fragment f) {

        dataSet = os_versions;
        this.context = context;
        this.f=f;
    }


    @Override
    public WinnerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
// create a new view
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.winner_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final WinnerAdapter.ViewHolder viewHolder, final int i) {

        winner fp = dataSet.get(i);

        viewHolder.title.setText(fp.getFirst_name()+" "+fp.getMiddle_name());
        viewHolder.type.setText(fp.getAward_type());
        viewHolder.round.setText(fp.getAward_round_title());




        String img= GlobalKeys.IMAGE_URL+fp.getUser_image();
        Picasso.with(context)
                .load(img)
                .into(viewHolder.iconView, new Callback() {
                    @Override
                    public void onSuccess() {
                        viewHolder.loader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        viewHolder.loader.setVisibility(View.GONE);
                    }
                });


        viewHolder.feed = fp;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView type;
        public CircularImageView iconView;
        public TextView round;
        public winner feed;
        public ProgressBar loader;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            title = (TextView) itemLayoutView
                    .findViewById(R.id.title);
            type = (TextView) itemLayoutView
                    .findViewById(R.id.type);
            round = (TextView) itemLayoutView
                    .findViewById(R.id.round);
            iconView = (CircularImageView) itemLayoutView
                    .findViewById(R.id.cover);

            loader = (ProgressBar) itemLayoutView.findViewById(R.id.loader);





        }

    }
}


