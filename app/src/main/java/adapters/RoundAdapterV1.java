package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ajlanawards.mivor.com.ajlanawards.R;
import models.round;

/**
 * Created by ahmed shabaan on 1/3/2017.
 */
public class RoundAdapterV1 extends ArrayAdapter<round> {

    private final Context context;
    private List<round> rounds;


    public RoundAdapterV1(Context context, List<round> rounds) {
        super(context, -1, rounds);
        this.context = context;
        this.rounds = rounds;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.round_item_v1, parent, false);
        TextView title = (TextView) rowView.findViewById(R.id.title);
        title.setText(rounds.get(position).getTitle());


        return rowView;
    }
}