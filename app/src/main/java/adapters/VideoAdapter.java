package adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import Accessors.GlobalKeys;
import ajlanawards.mivor.com.ajlanawards.R;
import ajlanawards.mivor.com.ajlanawards.Video_show;
import models.video;


public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {

    private static List<video> dataSet;
    public Context context;


    public VideoAdapter(List<video> os_versions, Context context) {

        dataSet = os_versions;
        this.context = context;
    }


    @Override
    public VideoAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
// create a new view
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.video_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final VideoAdapter.ViewHolder viewHolder, int i) {

        video fp = dataSet.get(i);

        viewHolder.title.setText(fp.getTitle());
        String[] link = fp.getUrl().split("=");
        String img= GlobalKeys.IMAGE_URL+fp.getThumbnail_image();
        Picasso.with(context)
                .load(img)
                .into(viewHolder.iconView, new Callback() {
                    @Override
                    public void onSuccess() {
                        viewHolder.loader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        viewHolder.loader.setVisibility(View.GONE);
                    }
                });


        viewHolder.feed = fp;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public ImageView iconView;
        public video feed;
        public ProgressBar loader;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            title = (TextView) itemLayoutView
                    .findViewById(R.id.title);
            iconView = (ImageView) itemLayoutView
                    .findViewById(R.id.cover);

            loader = (ProgressBar) itemLayoutView.findViewById(R.id.loader);

            iconView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.getContext().startActivity(new Intent(v.getContext(), Video_show.class).putExtra("id",feed.getUrl()));
                    }

            });




        }

    }
}

