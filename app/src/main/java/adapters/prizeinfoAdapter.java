package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import Accessors.CustomTextView;
import ajlanawards.mivor.com.ajlanawards.R;
import models.comunity;

/**
 * Created by ahmed on 28/11/16.
 */

public class prizeinfoAdapter extends ArrayAdapter<comunity> {

    private final Context context;
    private final List<comunity> values;
    int type ;
    int  index;

    public prizeinfoAdapter(Context context, List<comunity> values ,int type,int index) {
        super(context, -1,values);
        this.context = context;
        this.values = values;
        this.type = type;
        this.index = index;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.listmembercell, parent, false);

        if(type == 5 ){
            if(position==5  || position == 0){
                rowView = inflater.inflate(R.layout.divider, parent, false);
                if(position == 0){
                    ((TextView)rowView.findViewById(R.id.title)).setText("أعضاء لجنة جائزة القرأن الكريم");
                }
            }else {
                CustomTextView textView = (CustomTextView) rowView.findViewById(R.id.memberName);
                com.pkmmte.view.CircularImageView imageView = (CircularImageView) rowView.findViewById(R.id.memberImage);
                textView.setText(values.get(position).getName());
                Picasso.with(context)
                        .load(values.get(position).getImage())
                        .resize(200, 200)
                        .into(imageView);
            }
        }else if( type == 1){

            if(position == 0){
                rowView = inflater.inflate(R.layout.divider, parent, false);

            }else {
                CustomTextView textView = (CustomTextView) rowView.findViewById(R.id.memberName);
                com.pkmmte.view.CircularImageView imageView = (CircularImageView) rowView.findViewById(R.id.memberImage);
                textView.setText(values.get(position).getName());
                Picasso.with(context)
                        .load(values.get(position).getImage())
                        .resize(200, 200)
                        .into(imageView);
            }

        }else if( type == 2){

            if(position == 0){
                rowView = inflater.inflate(R.layout.divider, parent, false);

            }else {
                CustomTextView textView = (CustomTextView) rowView.findViewById(R.id.memberName);
                com.pkmmte.view.CircularImageView imageView = (CircularImageView) rowView.findViewById(R.id.memberImage);
                textView.setText(values.get(position).getName());
                Picasso.with(context)
                        .load(values.get(position).getImage())
                        .resize(200, 200)
                        .into(imageView);
            }

        }else if( type == 3){

            if(position == 0){
                rowView = inflater.inflate(R.layout.divider, parent, false);
                if(type == 3 ){

                    ((TextView)rowView.findViewById(R.id.title)).setText("أعضاء لجنة جائزة القرأن الكريم");
                }

            }else {
                CustomTextView textView = (CustomTextView) rowView.findViewById(R.id.memberName);
                com.pkmmte.view.CircularImageView imageView = (CircularImageView) rowView.findViewById(R.id.memberImage);
                textView.setText(values.get(position).getName());
                Picasso.with(context)
                        .load(values.get(position).getImage())
                        .resize(200, 200)
                        .into(imageView);
            }

        }else if( type == 4){

            if(position == 0){
                rowView = inflater.inflate(R.layout.divider, parent, false);

            }else {
                CustomTextView textView = (CustomTextView) rowView.findViewById(R.id.memberName);
                com.pkmmte.view.CircularImageView imageView = (CircularImageView) rowView.findViewById(R.id.memberImage);
                textView.setText(values.get(position).getName());
                Picasso.with(context)
                        .load(values.get(position).getImage())
                        .resize(200, 200)
                        .into(imageView);
            }

        }


        return rowView;
    }




}



