package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed shabaan on 1/3/2017.
 */
public class award_version {
    @SerializedName("title")
    String title;
    @SerializedName("image")
    String image;
    @SerializedName("pdf")
    String pdf;
    @SerializedName("created_date")
    String created_date;

    public award_version(String title, String image, String pdf, String created_date) {
        this.title = title;
        this.image = image;
        this.pdf = pdf;
        this.created_date = created_date;
    }

    public award_version() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }
}
