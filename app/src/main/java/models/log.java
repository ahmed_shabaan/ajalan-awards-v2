package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed shabaan on 1/3/2017.
 */
public class log {
    @SerializedName("name")
    String name;
    @SerializedName("phone")
    String phone;
    @SerializedName("comment")
    String  comment;

    public log() {
    }

    public log(String name, String phone, String comment) {
        this.name = name;
        this.phone = phone;
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
