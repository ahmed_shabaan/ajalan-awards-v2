package models;

import android.os.Bundle;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed shabaan on 1/2/2017.
 */
public class article {
    @SerializedName("title")
    String title;
    @SerializedName("image")
    String image;
    @SerializedName("body")
    String  body;

    public article(String title, String image, String body) {
        this.title = title;
        this.image = image;
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public article() {
    }

    public Bundle ToBundle(article n){

        Bundle b = new Bundle();
        b.putString("title", n.getTitle());
        b.putString("image", n.getImage());
        b.putString("body", n.getBody());


        return  b;
    }

    public article FromBundle(Bundle b){
        article n = new article(b.getString("title"),b.getString("image"),b.getString("body"));
        return  n;
    }
}
