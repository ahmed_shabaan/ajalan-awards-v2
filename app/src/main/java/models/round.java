package models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed shabaan on 12/20/2016.
 */
public class round  implements Parcelable{

    @SerializedName("nid")
    long id;
    @SerializedName("title")
    String title;

    public round(long id, String title) {
        this.id = id;
        this.title = title;
    }

    protected round(Parcel in) {
        id = in.readLong();
        title = in.readString();
    }

    public static final Creator<round> CREATOR = new Creator<round>() {
        @Override
        public round createFromParcel(Parcel in) {
            return new round(in);
        }

        @Override
        public round[] newArray(int size) {
            return new round[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(title);
    }
}
