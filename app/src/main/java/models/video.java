package models;


import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed shabaan on 12/15/2016.
 */
public class video {
    @SerializedName("title")
    String title;
    @SerializedName("video")
    String url;

    public String getThumbnail_image() {
        return thumbnail_image;
    }

    public void setThumbnail_image(String thumbnail_image) {
        this.thumbnail_image = thumbnail_image;
    }

    @SerializedName("thumbnail_image")

    String thumbnail_image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public video(String title, String url) {
        this.title = title;
        this.url = url;
    }

    public String getUrl() {
        return url;

    }

    public void setUrl(String url) {
        this.url = url;
    }
}
