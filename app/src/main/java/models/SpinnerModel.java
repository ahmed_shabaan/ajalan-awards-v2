package models;

/**
 * Created by ahmed shabaan on 12/25/2016.
 */
public class SpinnerModel {
    int key;
    String value;

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
