package models;

import android.os.Bundle;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed shabaan on 12/18/2016.
 */
public class news {
    @SerializedName("title")
    String title;
    @SerializedName("full_image")
    String full_image;
    @SerializedName("thumbnail_image")
    String thumbnail_image;
    @SerializedName("body")
    String body;

    public String getFull_image() {
        return full_image;
    }

    public void setFull_image(String full_image) {
        this.full_image = full_image;
    }

    public String getThumbnail_image() {
        return thumbnail_image;
    }

    public void setThumbnail_image(String thumbnail_image) {
        this.thumbnail_image = thumbnail_image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public news(String full_image, String thumbnail_image, String title, String body) {
        this.full_image = full_image;
        this.thumbnail_image = thumbnail_image;
        this.title = title;
        this.body = body;
    }

    public news() {
    }

    public Bundle ToBundle(news n){

         Bundle b = new Bundle();
         b.putString("title", n.getTitle());
         b.putString("full_image", n.getFull_image());
         b.putString("thumbnail_image", n.getThumbnail_image());
         b.putString("body", n.getBody());

        return  b;
    }

    public news FromBundle(Bundle b){
        news n = new news(b.getString("full_image"),b.getString("thumbnail_image"),b.getString("title"),b.getString("body"));
        return  n;
    }
}
