package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed shabaan on 12/27/2016.
 */
public class award {

   @SerializedName("node_title")
   String  node_title;
    @SerializedName("created_date")
   String created_date;
    @SerializedName("award_type")
   String award_type;
    @SerializedName("award_round")
   String award_round;
    @SerializedName("user_id")
   int user_id;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @SerializedName("status")

    String status;

    public String getNode_title() {
        return node_title;
    }

    public void setNode_title(String node_title) {
        this.node_title = node_title;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getAward_type() {
        return award_type;
    }

    public void setAward_type(String award_type) {
        this.award_type = award_type;
    }

    public String getAward_round() {
        return award_round;
    }

    public void setAward_round(String award_round) {
        this.award_round = award_round;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public award(String node_title, String created_date, String award_type, String award_round, int user_id,String status) {
        this.node_title = node_title;
        this.created_date = created_date;
        this.award_type = award_type;
        this.award_round = award_round;
        this.user_id = user_id;
        this.status = status;
    }
}
