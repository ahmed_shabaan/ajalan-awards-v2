package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed shabaan on 2/19/2017.
 */

public class active_awards {

    @SerializedName("nid")
    public long nid;
    @SerializedName("award_type_id")
    public int award_type_id;
    @SerializedName("title")
    public String title;

    public active_awards(long nid, int award_type, int award_type_id, String title) {
        this.nid = nid;
        this.award_type_id = award_type_id;
        this.title = title;
    }

    public long getNid() {
        return nid;
    }

    public void setNid(long nid) {
        this.nid = nid;
    }



    public int getAward_type_id() {
        return award_type_id;
    }

    public void setAward_type_id(int award_type_id) {
        this.award_type_id = award_type_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
