package models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ahmed shaban on 1/6/2018.
 */

public class SingleRegisterModel {


    long id;
    int award_type;

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAcademic() {
        return academic;
    }

    public void setAcademic(int academic) {
        this.academic = academic;
    }

    String firstname;
    String lastname;
    String thirdname;
    String fourthname;
    int academic;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getAward_type() {
        return award_type;
    }

    public void setAward_type(int award_type) {
        this.award_type = award_type;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMidname() {
        return lastname;
    }

    public void midLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getThirdname() {
        return thirdname;
    }

    public void setThirdname(String thirdname) {
        this.thirdname = thirdname;
    }

    public String getFourthname() {
        return fourthname;
    }

    public void setFourthname(String fourthname) {
        this.fourthname = fourthname;
    }

    public int getFamily_id() {
        return family_id;
    }

    public void setFamily_id(int family_id) {
        this.family_id = family_id;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public int getQablia_id() {
        return qablia_id;
    }

    public void setQablia_id(int qablia_id) {
        this.qablia_id = qablia_id;
    }

    public int getEducation_place() {
        return education_place;
    }

    public void setEducation_place(int education_place) {
        this.education_place = education_place;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNational_id() {
        return national_id;
    }

    public void setNational_id(String national_id) {
        this.national_id = national_id;
    }

    public image getFamilyCard() {
        return familyCard;
    }

    public void setFamilyCard(image familyCard) {
        this.familyCard = familyCard;
    }

    public image getAttach_degree() {
        return attach_degree;
    }

    public void setAttach_degree(image attach_degree) {
        this.attach_degree = attach_degree;
    }

    public image getAttach_degree_equals() {
        return attach_degree_equals;
    }

    public void setAttach_degree_equals(image attach_degree_equals) {
        this.attach_degree_equals = attach_degree_equals;
    }

    public image getUser_image() {
        return user_image;
    }

    public void setUser_image(image user_image) {
        this.user_image = user_image;
    }

    public image getIdenity_image() {
        return idenity_image;
    }

    public void setIdenity_image(image idenity_image) {
        this.idenity_image = idenity_image;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getEducation_level() {
        return education_level;
    }

    public void setEducation_level(int education_level) {
        this.education_level = education_level;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getQuean_parts() {
        return quean_parts;
    }

    public void setQuean_parts(int quean_parts) {
        this.quean_parts = quean_parts;
    }

    public int getEducation_stage() {
        return education_stage;
    }

    public void setEducation_stage(int education_stage) {
        this.education_stage = education_stage;
    }

    int family_id;
    int country_id;
    int qablia_id;
    int education_place;
    String city;
    String state;
    String national_id;
    image familyCard;
    image attach_degree;
    image attach_degree_equals;
    image user_image;
    image idenity_image;
    String birthDay;
    int sex;
    int education_level;
    String mobile;
    String  grade;
    String email;
    int quean_parts;
    int education_stage;



    public static class image {
        public  int id;
        public String url;
    }

}


