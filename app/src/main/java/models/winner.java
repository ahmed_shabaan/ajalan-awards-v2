package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed shabaan on 1/2/2017.
 */
public class winner {
    @SerializedName("first_name")
    String first_name;
    @SerializedName("fourth_name")
    String fourth_name;
    @SerializedName("last_name")
    String last_name;
    @SerializedName("middle_name")
    String middle_name;
    @SerializedName("birth_date")
    String birth_date;
    @SerializedName("award_type")
    String award_type;
    @SerializedName("award_id")
    long award_id;
    @SerializedName("round_id")
    long round_id;
    @SerializedName("award_round_title")
    String award_round_title;
    @SerializedName("award_type_id")
    long award_type_id;
    @SerializedName("user_image")
    String user_image;

    public winner(String first_name, String fourth_name, String last_name, String middle_name, String birth_date, String award_type, long award_id, long round_id, String award_round_title, long award_type_id, String user_image) {
        this.first_name = first_name;
        this.fourth_name = fourth_name;
        this.last_name = last_name;
        this.middle_name = middle_name;
        this.birth_date = birth_date;
        this.award_type = award_type;
        this.award_id = award_id;
        this.round_id = round_id;
        this.award_round_title = award_round_title;
        this.award_type_id = award_type_id;
        this.user_image = user_image;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getFourth_name() {
        return fourth_name;
    }

    public void setFourth_name(String fourth_name) {
        this.fourth_name = fourth_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getAward_type() {
        return award_type;
    }

    public void setAward_type(String award_type) {
        this.award_type = award_type;
    }

    public long getAward_id() {
        return award_id;
    }

    public void setAward_id(long award_id) {
        this.award_id = award_id;
    }

    public long getRound_id() {
        return round_id;
    }

    public void setRound_id(long round_id) {
        this.round_id = round_id;
    }

    public String getAward_round_title() {
        return award_round_title;
    }

    public void setAward_round_title(String award_round_title) {
        this.award_round_title = award_round_title;
    }

    public long getAward_type_id() {
        return award_type_id;
    }

    public void setAward_type_id(long award_type_id) {
        this.award_type_id = award_type_id;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }
}
