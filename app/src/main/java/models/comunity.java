package models;

/**
 * Created by ahmed shabaan on 1/11/2017.
 */

public class comunity {
    String name;
    int image;

    public comunity(String name, int image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
