package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ahmed shaban on 1/6/2018.
 */

public class RegistrationHistoryResponse {

    @SerializedName("nid")
    public int nid;
    @SerializedName("title")
    public String title;
    @SerializedName("award")
    public String award;
    @SerializedName("post date")
    public String post_date;
    @SerializedName("updated date")
    public String updated_date;
}
