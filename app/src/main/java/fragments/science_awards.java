package fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import Accessors.GlobalKeys;
import Accessors.PrefManager;
import ajlanawards.mivor.com.ajlanawards.Login;
import ajlanawards.mivor.com.ajlanawards.MainActivity;
import ajlanawards.mivor.com.ajlanawards.R;
import fragments.awards_values;

public class science_awards extends Fragment {
    RelativeLayout open_awards;
    RelativeLayout rules;
    RelativeLayout awards_rules,register;
    View content_layout;
    boolean isOpen = false;
    FragmentManager manager;
    FragmentTransaction transaction;
    public science_awards() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager =getActivity().getSupportFragmentManager();
        transaction = manager.beginTransaction();
        setHasOptionsMenu(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.content_science_awards, container, false);
        open_awards = (RelativeLayout) view.findViewById(R.id.open_awards_values);
        awards_rules = (RelativeLayout) view.findViewById(R.id.awards_rules);
        final int index = getArguments().getInt("index");
        rules = (RelativeLayout) view.findViewById(R.id.awards);
        content_layout = view.findViewById(R.id.content_layout);
        final awards_values awards_values = new awards_values();
        Bundle b = new Bundle();
        b.putInt("index", index);
        awards_values.setArguments(b);
        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
        if(index==1){

            main_title.setText(R.string.award_1);
        }else if(index==2){
            main_title.setText(R.string.award_2);
        }else if(index==3){
            main_title.setText(R.string.award_3);
        }else if(index==4){
            main_title.setText(R.string.award_4);
        }
        open_awards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOpen) {
                    isOpen = false;
                    getChildFragmentManager().beginTransaction().remove(awards_values).commit();

                } else {
                    getChildFragmentManager().beginTransaction().add(R.id.content_layout, awards_values, null).commit();
                    isOpen = true;
                }
            }
        });
        transaction = manager.beginTransaction();
        rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                prizeComunity prizeComunity = new prizeComunity();
                Bundle b = new Bundle();
                b.putInt("index", index);
                prizeComunity.setArguments(b);
                transaction.replace(R.id.myfrgmentlayout, prizeComunity, "vedioGallaryfragment");
                transaction.addToBackStack("Home");
                transaction.commit();
            }
        });

        awards_rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                awards_roles awards_roles = new awards_roles();
                Bundle b = new Bundle();
                b.putInt("index", index);
                awards_roles.setArguments(b);
                transaction.replace(R.id.myfrgmentlayout, awards_roles, "awards_roles");
                transaction.addToBackStack("awards_roles");
                transaction.commit();
            }
        });


        register = (RelativeLayout) view.findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefManager prefManager = new PrefManager(getActivity());

                if(prefManager.check_login()){
                    award_registration award_registration = new award_registration();
                    Bundle b = new Bundle();
                    b.putInt("index", index);
                    award_registration.setArguments(b);
                    transaction.replace(R.id.myfrgmentlayout, award_registration, "award_registration");
                    transaction.addToBackStack("award_registration");
                    transaction.commit();
                }else{
                    Intent intent = new Intent(getActivity(),Login.class);
                    startActivityForResult(intent, GlobalKeys.LOGIN_AND_REGISTER_REQUEST);
                    Toast.makeText(getActivity(), R.string.login_first, Toast.LENGTH_SHORT).show();
                }

            }
        });

        return  view;
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }




}
