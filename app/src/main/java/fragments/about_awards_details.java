package fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ajlanawards.mivor.com.ajlanawards.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link about_awards_details.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link about_awards_details#newInstance} factory method to
 * create an instance of this fragment.
 */
public class about_awards_details extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public about_awards_details() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment about_awards_details.
     */
    // TODO: Rename and change types and number of parameters
    public static about_awards_details newInstance(String param1, String param2) {
        about_awards_details fragment = new about_awards_details();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View   view =  inflater.inflate(R.layout.fragment_about_awards_details, container, false);;
        int index = getArguments().getInt("index");

        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);

        if(index==1){
            main_title.setText(getString(R.string.award_idea));
            view =  inflater.inflate(R.layout.fragment_about_awards_details, container, false);
        }else if(index == 2){
            main_title.setText(getString(R.string.award_goals));
            view =  inflater.inflate(R.layout.award_goals, container, false);
        }else if(index == 3){
            main_title.setText(getString(R.string.award_vision));
            view =  inflater.inflate(R.layout.award_vision, container, false);
        }else if(index == 5){
            main_title.setText(getString(R.string.award_mechanism));
            view =  inflater.inflate(R.layout.how_to_apply, container, false);
        }

        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }
}
