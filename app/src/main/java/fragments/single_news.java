package fragments;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import Accessors.GlobalKeys;
import ajlanawards.mivor.com.ajlanawards.MainActivity;
import ajlanawards.mivor.com.ajlanawards.R;
import models.*;
import models.news;

/**
 * Created by ahmed shabaan on 12/19/2016.
 */
public class single_news extends Fragment {



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_news_detail, container, false);
        Bundle b = getArguments();
        models.news item  = new news();
        item = item.FromBundle(b);
        TextView title = (TextView) view.findViewById(R.id.title);
        TextView body = (TextView) view.findViewById(R.id.body);
        ImageView cover = (ImageView) view.findViewById(R.id.cover);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.loader);

        title.setText(item.getTitle());
        body.setText(Html.fromHtml(item.getBody()).toString());

        String img= GlobalKeys.IMAGE_URL+item.getThumbnail_image();
        Picasso.with(getActivity())
                .load(img)
                .into(cover, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);
                    }
                });



        return view;
    }




    public single_news() {}


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }


}

