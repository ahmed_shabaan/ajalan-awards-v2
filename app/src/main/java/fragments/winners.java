package fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Accessors.GlobalKeys;
import ApiClient.ApiClient;
import adapters.NewsAdaptor;
import adapters.WinnerAdapter;
import ajlanawards.mivor.com.ajlanawards.R;
import models.*;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link winners.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link winners#newInstance} factory method to
 * create an instance of this fragment.
 */
public class winners extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    long id  = 0;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    List<winner> winnerList = new ArrayList<>();
    RecyclerView recyclerView;
    WinnerAdapter winnerAdapter;
    ProgressBar progressBar;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment winners.
     */
    // TODO: Rename and change types and number of parameters
    public static winners newInstance(String param1, String param2) {
        winners fragment = new winners();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public winners() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  =  inflater.inflate(R.layout.fragment_winners, container, false);
        id = getArguments().getLong("index");
        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
        main_title.setText(getString(R.string.winners));
        progressBar = (ProgressBar) view.findViewById(R.id.loader);
        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1, 1, false));
        get_winner(id);
        return  view;
    }

    private void get_winner(long id) {
        Call<List<winner>> call = ApiClient.getApiInterface().getWinners(GlobalKeys.get_lang(getActivity()), id);

        call.enqueue(new Callback<List<winner>>() {
            @Override
            public void onResponse(Call<List<winner>> call, Response<List<winner>> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null && response.body().size() > 0) {
                    winnerList = response.body();
                    winnerAdapter = new WinnerAdapter(winnerList,getActivity(),winners.this);
                    recyclerView.setAdapter(winnerAdapter);
                } else {
                    Toast.makeText(getActivity(), R.string.no_winners_found, Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<winner>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.internet_error), Toast.LENGTH_SHORT).show();

                Log.d("ss", t.toString());
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }

}
