package fragments;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import Accessors.GlobalKeys;
import Accessors.PrefManager;
import ApiClient.ApiClient;
import ApiClient.ApiInterface;
import adapters.SpinAdapter;
import ajlanawards.mivor.com.ajlanawards.R;
import models.SpinnerModel;
import models.user;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link profile.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link profile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class profile extends android.support.v4.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    long user_id;
    EditText first_name,last_name,mid_name,mobile,national_id;
    TextView data_of_birth;
    ImageView logo;
    long cer_id = 0 ;
    int year,month,day;
    String Image_base_64  = null;
    Button upload_file;
    List<SpinnerModel> gender_values ;
    SpinAdapter gender_adapter;
    Spinner gender;
    long family_id = 0;
    long gender_id  = 0;
    long education_id = 0;
    Button edit;
    String update_profile;

    List<SpinnerModel> education_values ;
    SpinAdapter education_adapter;
    Spinner education;

    List<SpinnerModel> family_values ;
    SpinAdapter family_adapter;
    Spinner family;
    user u;
    ProgressBar progressBar;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private EditText fourth_name;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment profile.
     */
    // TODO: Rename and change types and number of parameters
    public static profile newInstance(String param1, String param2) {
        profile fragment = new profile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public profile() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
        main_title.setText(R.string.profile);
         first_name = (EditText) view.findViewById(R.id.fname);
        mid_name= (EditText) view.findViewById(R.id.middlename);
        last_name = (EditText) view.findViewById(R.id.lastname);
        fourth_name = (EditText) view.findViewById(R.id.fourth_name);
        mobile= (EditText) view.findViewById(R.id.mobilenumer);
        national_id= (EditText) view.findViewById(R.id.national_id);
        logo = (ImageView) view.findViewById(R.id.logo);
        upload_file = (Button) view.findViewById(R.id.upload_file);
        data_of_birth = (TextView) view.findViewById(R.id.date_of_birth);
        edit = (Button) view.findViewById(R.id.edit);
        progressBar  = (ProgressBar) view.findViewById(R.id.loader);

        gender_values = new ArrayList<>();
        SpinnerModel spinnerModel = new SpinnerModel();
        spinnerModel.setKey(0);
        spinnerModel.setValue("-" + getString(R.string.sex) + "-");
        gender_values.add(spinnerModel);
         spinnerModel = new SpinnerModel();
        spinnerModel.setKey(76);
        spinnerModel.setValue(getString(R.string.radio_male));
        gender_values.add(spinnerModel);
         spinnerModel = new SpinnerModel();
        spinnerModel.setKey(77);
        spinnerModel.setValue(getString(R.string.radio_female));
        gender_values.add(spinnerModel);
        gender = (Spinner) view.findViewById(R.id.gender);
        gender_adapter = new SpinAdapter(getActivity(),
                android.R.layout.simple_spinner_item, gender_values);
        gender_adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        gender.setAdapter(gender_adapter);


        education_values  = new ArrayList<>();
        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(0);
        spinnerModel.setValue("-"+getString(R.string.eduction_level)+"-");
        education_values.add(spinnerModel);
        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(79);
        spinnerModel.setValue(getString(R.string.first_class));
        education_values.add(spinnerModel);
        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(80);
        spinnerModel.setValue(getString(R.string.second_class));
        education_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(81);
        spinnerModel.setValue(getString(R.string.third_class));
        education_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(87);
        spinnerModel.setValue(getString(R.string.high_first));
        education_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(88);
        spinnerModel.setValue(getString(R.string.high_second));
        education_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(89);
        spinnerModel.setValue(getString(R.string.high_third));
        education_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(90);
        spinnerModel.setValue(getString(R.string.undergraduate));
        education_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(91);
        spinnerModel.setValue(getString(R.string.graduated));
        education_values.add(spinnerModel);


        education = (Spinner) view.findViewById(R.id.education_level);
        education_adapter = new SpinAdapter(getActivity(),
                android.R.layout.simple_spinner_item, education_values);
        education_adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        education.setAdapter(education_adapter);
        family_values = new ArrayList<>();
        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(0);
        spinnerModel.setValue("-"+getString(R.string.family)+"-");
        family_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(82);
        spinnerModel.setValue(getString(R.string.ajalan));
        family_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(83);
        spinnerModel.setValue(getString(R.string.el3ed));
        family_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(84);
        spinnerModel.setValue(getString(R.string.eltwel));
        family_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(85);
        spinnerModel.setValue(getString(R.string.brothers_of_ebn_fahd));
        family_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(86);
        spinnerModel.setValue(getString(R.string.sons_of_abdel_azez));
        family_values.add(spinnerModel);

        family = (Spinner) view.findViewById(R.id.family);
        family_adapter = new SpinAdapter(getActivity(),
                android.R.layout.simple_spinner_item, family_values);
        family_adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        family.setAdapter(family_adapter);








        PrefManager prefManager = new PrefManager(getActivity());
        user_id = prefManager.getUserId();
        new HttpRequestTaskUser().execute(user_id);
        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    private class HttpRequestTaskUser extends AsyncTask<Long, String ,String> {
        public ProgressDialog progress;
        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(getActivity());
            progress.setMessage(getString(R.string.loading));
            progress.setCancelable(false);
            progress.show();
        }

        @Override
        protected String doInBackground(Long... params) {

            String content = null;

            try {

                String InsertTransactionResult = null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpParams myParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(myParams, 100000);
                HttpConnectionParams.setSoTimeout(myParams, 100000);

                try {

                    PrefManager prefManager = new PrefManager(getActivity());
                    String mUsername = prefManager.getUsername();
                    String mPassword = prefManager.getPassword();
                    HttpGet httppost = new HttpGet(GlobalKeys.GLBURL+GlobalKeys.get_user_by_id+params[0]+".json");
                    httppost.setHeader("Content-type", "application/json");

                    String base64EncodedCredentials = "Basic " + Base64.encodeToString(
                            (mUsername + ":" + mPassword).getBytes(),
                            Base64.NO_WRAP);
                    httppost.setHeader("Authorization", base64EncodedCredentials);

                   /* StringEntity se = new StringEntity(params[0]);*/

                   /* se.setContentEncoding(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE,
                            "application/json"));
                    httppost.setEntity(se);*/

                    HttpResponse response = httpclient.execute(httppost);

                     content = EntityUtils
                            .toString(response.getEntity());

                    Log.d("Response" , ""+content);

                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                }

            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return content;



        }


        @Override
        protected void onPostExecute(String content) {
            progress.dismiss();
            if(content!=null && content.contains("uid")){
                try {
                    u= new user();
                    JSONObject jsonObject = new JSONObject(content);
                    u.setId(jsonObject.getLong("uid"));
                    u.setName(jsonObject.getString("name"));
                    u.setEmail(jsonObject.getString("mail"));
                    Object intervention = jsonObject.get("field_attach_identity");
                    if(intervention instanceof JSONObject){
                        u.setIdentity((jsonObject.getJSONObject("field_attach_identity")).getJSONArray("und").getJSONObject(0).getLong("fid"));
                    }else{
                        u.setIdentity(0);

                    }
                     intervention = jsonObject.get("field_user_image");

                    if(intervention instanceof JSONObject){
                        String len = ((JSONObject) intervention).getString("und");
                        if(!len.equals("null")){
                            u.setPicture_id((jsonObject.getJSONObject("field_user_image")).getJSONArray("und").getJSONObject(0).getString("fid"));
                            u.setPic_url((jsonObject.getJSONObject("field_user_image")).getJSONArray("und").getJSONObject(0).getString("url"));

                        }else{
                            u.setPicture_id("");
                            u.setPic_url("");
                        }

                    }else{
                        u.setPicture_id("");
                        u.setPic_url("");
                    }
                    u.setFirst_name((jsonObject.getJSONObject("field_first_name")).getJSONArray("und").getJSONObject(0).getString("value"));
                    u.setMed_name((jsonObject.getJSONObject("field_middle_name")).getJSONArray("und").getJSONObject(0).getString("value"));
                    u.setLast_name((jsonObject.getJSONObject("field_last_name")).getJSONArray("und").getJSONObject(0).getString("value"));

                    intervention = jsonObject.get("field_fourth_name");
                    if(intervention instanceof JSONObject){
                        u.setForth_name((jsonObject.getJSONObject("field_fourth_name")).getJSONArray("und").getJSONObject(0).getString("value"));

                    }else{
                        u.setForth_name("");
                    }
                    u.setMobile((jsonObject.getJSONObject("field_mobile_number")).getJSONArray("und").getJSONObject(0).getString("value"));
                    intervention = jsonObject.get("field_educational_level");
                    if(intervention instanceof JSONObject){
                        u.setEducation((jsonObject.getJSONObject("field_educational_level")).getJSONArray("und").getJSONObject(0).getInt("tid"));

                    }else{
                        u.setEducation(0);
                    }
                    intervention = jsonObject.get("field_gender");
                    if(intervention instanceof JSONObject){
                        u.setGender((jsonObject.getJSONObject("field_gender")).getJSONArray("und").getJSONObject(0).getInt("tid"));

                    }else{
                        u.setGender(0);
                    }

                    intervention = jsonObject.get("field_family");
                    if(intervention instanceof JSONObject){
                        u.setFamily((jsonObject.getJSONObject("field_family")).getJSONArray("und").getJSONObject(0).getInt("tid"));

                    }else{
                        u.setFamily(0);
                    }

                    intervention = jsonObject.get("field_national_id");
                    if(intervention instanceof JSONObject){
                        u.setNational_id((jsonObject.getJSONObject("field_national_id")).getJSONArray("und").getJSONObject(0).getString("value"));

                    }else{
                        u.setNational_id("");
                    }
                    u.setDate_of_birth((jsonObject.getJSONObject("field_birth_date")).getJSONArray("und").getJSONObject(0).getString("value"));
                    update_ui(u);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(getActivity(), R.string.try_again, Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void update_ui(final user u) {
        first_name.setText(u.getFirst_name());
        last_name.setText(u.getLast_name());
        mid_name.setText(u.getMed_name());

        mobile.setText(u.getMobile());
        national_id.setText(u.getNational_id());
        if(u.getPicture_id().length() > 0){
            cer_id = Long.parseLong(u.getPicture_id());
        }
        String string = u.getDate_of_birth();
        String[] parts = string.split(" ");
        String part1 = parts[0]; // 004
        data_of_birth.setText(part1);

        family_id =  u.getFamily();
        education_id = u.getEducation();
        gender_id = u.getGender();



        String img= GlobalKeys.IMAGE_URL+u.getPic_url();
        Picasso.with(getActivity())
                .load(img)
                .placeholder(R.drawable.plus)
                .into(logo, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);

                    }
                });

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    GlobalKeys.selectImage(getActivity());

                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        GlobalKeys.selectImage(getActivity());
                    } else {


                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},23
                        );

                    }
                }else{
                    GlobalKeys.selectImage(getActivity());

                }

            }
        });

        upload_file.setTypeface(GlobalKeys.getFont(getActivity()));
        upload_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Image_base_64 != null) {
                    JSONArray a = new JSONArray();
                    JSONObject jsonObject = new JSONObject();
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    try {
                        jsonObject.put("file", Image_base_64);
                        jsonObject.put("filename", "change primary domain chat " + ts + ".jpg");
                        jsonObject.put("filename", "sites/default/files/change primary domain chat " + ts + ".jpg");
                        String main = jsonObject.toString();
                        //upload_file(loginService, main);
                        new HttpRequestTask().execute(main);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), R.string.select_profile_image, Toast.LENGTH_SHORT).show();
                }
            }
        });

        gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //date view
        final Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(calendar);
            }

        };

        data_of_birth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        for(int i = 0 ; i < family_values.size();i++){
            SpinnerModel spinnerModel = family_values.get(i);
            if(spinnerModel.getKey()==u.getFamily()){
                family_id = u.getFamily();
                family.setSelection(i);
                break;
            }
        }

        family.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position!=0){
                    family_id = family_values.get(position).getKey();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        family_id = u.getFamily();



        //gender detection

        for(int i = 0 ; i < gender_values.size();i++){
            SpinnerModel spinnerModel = gender_values.get(i);
            if(spinnerModel.getKey()==u.getGender()){
                gender_id = u.getGender();
                gender.setSelection(i);
                break;
            }
        }

        gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position!=0){
                    gender_id = gender_values.get(position).getKey();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        gender_id = u.getGender();


        //education detection

        for(int i = 0 ; i < education_values.size();i++){
            SpinnerModel spinnerModel = education_values.get(i);
            if(spinnerModel.getKey()==u.getEducation()){
                education_id = u.getEducation();
                education.setSelection(i);
                break;
            }
        }

        education.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    education_id = education_values.get(position).getKey();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        education_id = u.getEducation();


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                first_name.setError(null);
                last_name.setError(null);
                mobile.setError(null);
                national_id.setError(null);
                data_of_birth.setError(null);

                mid_name.setError(null);

                String fname = first_name.getText().toString().trim();
                String flast = last_name.getText().toString().trim();
                String Mmobile = mobile.getText().toString().trim();
                String Mnational = national_id.getText().toString().trim();
                String date = data_of_birth.getText().toString().trim();
                String _mid_name = mid_name.getText().toString().trim();
                if(TextUtils.isEmpty(fname)){
                    first_name.setError(getString(R.string.error_field_required));
                }else if(TextUtils.isEmpty(flast)){
                    last_name.setError(getString(R.string.error_field_required));
                }else if(TextUtils.isEmpty(_mid_name)) {
                    mid_name.setError(getString(R.string.error_field_required));

                }else if(TextUtils.isEmpty(Mmobile)){
                    mobile.setError(getString(R.string.error_field_required));
                }else if(TextUtils.isEmpty(Mnational)){
                    national_id.setError(getString(R.string.error_field_required));
                }else if(TextUtils.isEmpty(date)){
                    data_of_birth.setError(getString(R.string.error_field_required));
                }
                else if(gender_id ==0){
                    Toast.makeText(getActivity(), R.string.gender_error, Toast.LENGTH_SHORT).show();
                }else if(family_id==0){
                    Toast.makeText(getActivity(), R.string.family_select, Toast.LENGTH_SHORT).show();
                }else{
                    String _none = "_none";
                    update_profile = "\n" +
                            "{\n" +
                            "  \"name\": \""+u.getName()+"\",\n" +
                            "  \"mail\": \""+u.getEmail()+"\",\n" +
                            "  \"field_first_name\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+fname+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            " \"field_fourth_name\": {" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \"_none\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_middle_name\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+mid_name.getText().toString().trim()+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_user_image\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"fid\": \""+cer_id+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_birth_date\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": {\"date\":\""+data_of_birth.getText().toString()+"\"}\n" +
                            "      }]\n" +
                            "  },\n" +
                            "  \"field_last_name\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+flast+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_mobile_number\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+mobile.getText().toString()+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_educational_level\": {\n" +
                            "    \"und\": \n" +
                            "      {\n" +
                            "        \"tid\": \""+_none+"\"\n" +
                            "      }\n" +
                            "    \n" +
                            "  },\n" +
                            "  \"field_gender\": {\n" +
                            "    \"und\": \n" +
                            "      {\n" +
                            "        \"value\": \""+gender_id+"\"\n" +
                            "      }\n" +
                            "  },\n" +
                            "  \"field_family\": {\n" +
                            "    \"und\": \n" +
                            "      {\n" +
                            "        \"value\": \""+family_id+"\"\n" +
                            "      }\n" +
                            "    \n" +
                            "  },\n" +
                            "  \"field_national_id\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+national_id.getText().toString()+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  }\n" +
                            "}";

                    new HttpRequestTaskUserUpdateProfile().execute(user_id);
                }
            }
        });


    }

    private void updateLabel(Calendar ca) {

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        data_of_birth.setText(format1.format(ca.getTime()));
    }

    String  Result;
    private class HttpRequestTask extends AsyncTask<String, Void ,Void> {
        public ProgressDialog progress;
        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(getActivity());
            progress.setMessage(getString(R.string.loading));
            progress.setCancelable(false);
            progress.show();
        }

        @Override
        protected Void doInBackground(String... params) {



            try {

                String InsertTransactionResult = null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpParams myParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(myParams, 100000);
                HttpConnectionParams.setSoTimeout(myParams, 100000);

                try {

                    PrefManager prefManager = new PrefManager(getActivity());
                    String mUsername = prefManager.getUsername();
                    String mPassword = prefManager.getPassword();
                    HttpPost httppost = new HttpPost(GlobalKeys.GLBURL+"test/file.json");
                    httppost.setHeader("Content-type", "application/json");

                    String base64EncodedCredentials = "Basic " + Base64.encodeToString(
                            (mUsername + ":" + mPassword).getBytes(),
                            Base64.NO_WRAP);
                    httppost.setHeader("Authorization", base64EncodedCredentials);

                    StringEntity se = new StringEntity(params[0]);

                    se.setContentEncoding(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE,
                            "application/json"));
                    httppost.setEntity(se);

                    HttpResponse response = httpclient.execute(httppost);

                    Result = EntityUtils
                            .toString(response.getEntity());

                    Log.d("Response" , ""+Result);

                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                }

            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;



        }


        @Override
        protected void onPostExecute(Void aVoid) {
            progress.dismiss();
            if(Result.contains("fid")){
                try {
                    JSONObject jsonObject = new JSONObject(Result);
                    cer_id = jsonObject.getLong("fid");
                    Toast.makeText(getActivity(),"تم رفع الصورة بنجاح", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(getActivity(), R.string.try_again, Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Image_base_64 = GlobalKeys.ImageResult(requestCode,data,logo,getActivity());
        }
    }


    private class HttpRequestTaskUserUpdateProfile extends AsyncTask<Long, String ,String> {
        public ProgressDialog progress;
        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(getActivity());
            progress.setMessage(getString(R.string.loading));
            progress.setCancelable(false);
            progress.show();
        }

        @Override
        protected String doInBackground(Long... params) {

            String content = null;

            try {

                String InsertTransactionResult = null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpParams myParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(myParams, 100000);
                HttpConnectionParams.setSoTimeout(myParams, 100000);

                try {

                    PrefManager prefManager = new PrefManager(getActivity());
                    String mUsername = prefManager.getUsername();
                    String mPassword = prefManager.getPassword();
                    HttpPut httppost = new HttpPut(GlobalKeys.GLBURL+GlobalKeys.get_user_by_id+params[0]+".json");
                    httppost.setHeader("Content-type", "application/json");

                    String base64EncodedCredentials = "Basic " + Base64.encodeToString(
                            (mUsername + ":" + mPassword).getBytes(),
                            Base64.NO_WRAP);
                    httppost.setHeader("Authorization", base64EncodedCredentials);
                    StringEntity se = new StringEntity(update_profile, HTTP.UTF_8);

                    se.setContentEncoding(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE,
                            "application/json"));
                    httppost.setEntity(se);
                    HttpResponse response = httpclient.execute(httppost);

                    content = EntityUtils
                            .toString(response.getEntity());

                    Log.d("Response" , ""+content);

                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                }

            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return content;



        }


        @Override
        protected void onPostExecute(String content) {
            progress.dismiss();
            if(content!=null && content.contains("uid")){
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack();
                Toast.makeText(getActivity(), R.string.success_profile_edit, Toast.LENGTH_SHORT).show();


            }else{
                Toast.makeText(getActivity(), R.string.try_again, Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }


}
