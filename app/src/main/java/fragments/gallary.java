package fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Accessors.GlobalKeys;
import Accessors.ImageItem;
import adapters.GridViewAdapter;
import ajlanawards.mivor.com.ajlanawards.DetailsActivity;
import ajlanawards.mivor.com.ajlanawards.MainActivity;
import ajlanawards.mivor.com.ajlanawards.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link gallary.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link gallary#newInstance} factory method to
 * create an instance of this fragment.
 */
public class gallary extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String  Result;
    final ArrayList<ImageItem> imageItems = new ArrayList<>();
    ProgressBar progressBar;
    android.app.FragmentManager manager;
    FragmentTransaction transaction;
    android.support.v4.app.Fragment list;

    private OnFragmentInteractionListener mListener;

    private GridView gridView;
    private GridViewAdapter gridAdapter;


    public gallary() {


        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment gallary.
     */
    // TODO: Rename and change types and number of parameters
    public static gallary newInstance() {
        gallary fragment = new gallary();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_gallary, container, false);
        final Context context = view.getContext();
        long id = getArguments().getLong("index",0);
        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
        main_title.setText(R.string.gallary);
        gridView = (GridView) view.findViewById(R.id.gridView);
        progressBar = (ProgressBar) view.findViewById(R.id.loader);
        new HttpRequestTask().execute(id);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                ImageItem item = (ImageItem) parent.getItemAtPosition(position);
                //Create intent
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("title", item.getTitle());
                intent.putExtra("fullurl", item.getFullURL());
//                intent.putExtra("image", item.getImage());
                //Start details activity
                getActivity().startActivity(intent);
              //  getActivity().finish();
            }
        });
        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private ArrayList<ImageItem> getData() {
        final ArrayList<ImageItem> imageItems = new ArrayList<>();

        return imageItems;
    }





    private class HttpRequestTask extends AsyncTask<Long, Void ,Void> {


        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Long... params2) {


            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, "UTF-8");
            params.setBooleanParameter("http.protocol.expect-continue", false);
            HttpClient httpclient = new DefaultHttpClient(params);

            HttpGet httpget = new HttpGet(GlobalKeys.GETPHOTOURL+"?args[0]="+params2[0]);
            try
            {
                HttpResponse response = httpclient.execute(httpget);
                HttpEntity entity = response.getEntity();
                Result = EntityUtils.toString(entity, HTTP.UTF_8);
                // ...
                JSONArray json = new JSONArray(Result);
                for(int i=0;i<json.length();i++){
                    JSONObject e = json.getJSONObject(i);
                    ImageItem imageItemCell = new ImageItem();
                    imageItemCell.setTitle(e.getString("title"));
                    imageItemCell.setFullURL(GlobalKeys.IMAGE_URL+e.getString("full_image"));
                    imageItemCell.setThumbURL(GlobalKeys.IMAGE_URL+e.getString("thumbnail_image"));
                    imageItems.add(imageItemCell);


                }
            }
            catch (Exception e)
            {
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressBar.setVisibility(View.GONE);
            gridAdapter = new GridViewAdapter(getActivity(), R.layout.gallaryitem, imageItems);
            gridView.setAdapter(gridAdapter);

        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }




}
