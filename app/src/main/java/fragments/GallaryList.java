package fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Accessors.CustomTextView;
import Accessors.GlobalKeys;
import Accessors.ImageItem;
import Accessors.SimpleDividerItemDecoration;
import Accessors.utils;
import ApiClient.ApiClient;
import adapters.NewsAdaptor;
import adapters.RoundAdapter;
import adapters.RoundAdapterGallaryList;
import ajlanawards.mivor.com.ajlanawards.DetailsActivity;
import ajlanawards.mivor.com.ajlanawards.Login;
import ajlanawards.mivor.com.ajlanawards.R;
import models.round;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GallaryList.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GallaryList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GallaryList extends Fragment {
    private static final int REQ_CODE = 99;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    FragmentManager manager;
    FragmentTransaction transaction;
    ProgressBar loader;

    ArrayList<round> roundList = new ArrayList<>();
    /*ListView videos_round;
    ListView photos_round;
    ListView articles_round;
    ListView news_round;*/
    RoundAdapterGallaryList roundAdapterGallaryListVideo,roundAdapterGallaryListImages,roundAdapterGallaryListArticles,roundAdapterGallaryListVideonews;
    RoundAdapter roundAdapter;
    ImageView news,articles,versions,photos,videos;
    RecyclerView videos_round;
    RecyclerView news_round;
    RecyclerView articles_round;
    RecyclerView photos_round;
    private static final int ARTICLES = 655;
    private static final int IMAGES = 65;
    private static final int VEDIOS = 6;
    private static final int NEWS = 5;

    int type = 0 ;



    private OnFragmentInteractionListener mListener;



    public GallaryList() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters
     * @return A new instance of fragment GallaryList.
     */
    // TODO: Rename and change types and number of parameters
    public static GallaryList newInstance() {
        GallaryList fragment = new GallaryList();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {;
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_gallary_list, container, false);
        final Context context = view.getContext();
        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
        main_title.setText(getString(R.string.menue_photoGallary));
        manager  = getFragmentManager();
        news = view.findViewById(R.id.news);
        photos = view.findViewById(R.id.photos);
        articles = view.findViewById(R.id.articles);
        videos = view.findViewById(R.id.videos);

        versions = view.findViewById(R.id.versions);

        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = GlobalKeys.news;
                ValidateDialog();
            }
        });

        articles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = GlobalKeys.articles;
                ValidateDialog();
            }
        });


        photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = GlobalKeys.images;
                ValidateDialog();
            }
        });

        videos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = GlobalKeys.vedio;
                ValidateDialog();
            }
        });








        versions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transaction = manager.beginTransaction();
                awards_issuse awards_issuse = new awards_issuse();
                //   menuItemOne.setArguments(b);
                transaction.hide(GallaryList.this);
                transaction.add(R.id.myfrgmentlayout, awards_issuse, "awards_issuse");
                transaction.addToBackStack("awards_issuse");
                transaction.commit();
            }
        });

        return  view;
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void ValidateDialog(){
        if(roundList.size() > 0){
            showDialog(roundList);
        }else{
            get_rounds(GlobalKeys.news);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void get_rounds(final int type) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        Call<ArrayList<round>> listCall = ApiClient.getApiInterface().getRounds(GlobalKeys.get_lang(getActivity()));

        listCall.enqueue(new Callback<ArrayList<round>>() {
            @Override
            public void onResponse(Call<ArrayList<round>> call, Response<ArrayList<round>> response) {
                progressDialog.hide();
                if (response.body() != null) {
                    roundList = response.body();
                    showDialog(roundList);
                } else {
                    Toast.makeText(getActivity(), R.string.no_rounds, Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<ArrayList<round>> call, Throwable t) {
                Toast.makeText(getActivity(), R.string.internet_error, Toast.LENGTH_SHORT).show();
                progressDialog.hide();
                Log.d("ss", t.toString());
            }
        });
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }



    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
            main_title.setText(getString(R.string.menue_photoGallary));

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(REQ_CODE==requestCode && resultCode==1 && data!=null){
            filterSrFragment.dismiss();
            long id = data.getExtras().getLong("id");
            navigate(id);
        }

    }

    private void navigate(long id) {
         FragmentManager manager = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction;
                    transaction = manager.beginTransaction();
                    if(type == GlobalKeys.news){
                        news news = new news();
                        Bundle b = new Bundle();
                        b.putLong("index",id);
                        news.setArguments(b);
                        transaction.hide(this);
                        transaction.add(R.id.myfrgmentlayout, news, "vedioGallaryfragment");
                        transaction.addToBackStack("Home");
                        transaction.commit();
                    }else if(type == GlobalKeys.vedio){
                        vedioGallary vedioGallary = new vedioGallary();
                        Bundle b = new Bundle();
                        b.putLong("index",id);
                        vedioGallary.setArguments(b);
                        transaction.hide(this);
                        transaction.add(R.id.myfrgmentlayout, vedioGallary, "vedioGallaryfragment");
                        transaction.addToBackStack("Home");
                        transaction.commit();
                    }else if(type == GlobalKeys.articles){
                        articles articles = new articles();
                        Bundle b = new Bundle();
                        b.putLong("index",id);
                        articles.setArguments(b);
                        transaction.hide(this);
                        transaction.add(R.id.myfrgmentlayout, articles, "vedioGallaryfragment");
                        transaction.addToBackStack("Home");
                        transaction.commit();
                    }else if(type == GlobalKeys.images){
                        gallary gallary = new gallary();
                        Bundle b = new Bundle();
                        b.putLong("index",id);
                        gallary.setArguments(b);
                        transaction.hide(this);
                        transaction.add(R.id.myfrgmentlayout, gallary, "gallary");
                        transaction.addToBackStack("gallary");
                        transaction.commit();
                    }
    }

    roundDailogFragment filterSrFragment ;
    private  void  showDialog(ArrayList<round> roundList){
        filterSrFragment = new roundDailogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("list",roundList);
        filterSrFragment.setArguments(bundle);
        filterSrFragment.setTargetFragment(this, REQ_CODE);
        filterSrFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }
}
