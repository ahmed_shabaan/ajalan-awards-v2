package fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import Accessors.GlobalKeys;
import ajlanawards.mivor.com.ajlanawards.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link awards_roles.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link awards_roles#newInstance} factory method to
 * create an instance of this fragment.
 */
public class awards_roles extends android.support.v4.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment awards_roles.
     */
    // TODO: Rename and change types and number of parameters
    public static awards_roles newInstance(String param1, String param2) {
        awards_roles fragment = new awards_roles();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public awards_roles() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String lang = GlobalKeys.get_lang(getActivity());

        View view = null ;
        if(lang.equals("en")){
            view =inflater.inflate(R.layout.conditions_english, container, false);
        }else if(lang.equals("ar")){
            view =  inflater.inflate(R.layout.fragment_awards_roles, container, false);

        }
        ScrollView tab1 = (ScrollView) view.findViewById(R.id.tab1);
        ScrollView tab2 = (ScrollView) view.findViewById(R.id.tab2);
        ScrollView tab3 = (ScrollView) view.findViewById(R.id.tab3);
        ScrollView tab4 = (ScrollView) view.findViewById(R.id.tab4);

        int index = getArguments().getInt("index");
        if(index==1)
            tab1.setVisibility(View.VISIBLE);
            else if(index==2)
            tab2.setVisibility(View.VISIBLE);
            else if(index==3)
            tab3.setVisibility(View.VISIBLE);
            else if(index==4)
            tab4.setVisibility(View.VISIBLE);


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }

}
