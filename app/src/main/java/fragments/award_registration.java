package fragments;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Accessors.GlobalKeys;
import Accessors.PrefManager;
import Accessors.utils;
import ApiClient.ApiClient;
import ApiClient.ApiInterface;

import adapters.NewsAdaptor;
import adapters.SpinAdapter;
import ajlanawards.mivor.com.ajlanawards.R;
import models.SingleRegisterModel;
import models.SpinnerModel;
import models.active_awards;
import models.user;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

    /**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link award_registration.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link award_registration#newInstance} factory method to
 * create an instance of this fragment.
 */
public class award_registration extends android.support.v4.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    long index;
    long award_type;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    EditText first_name,last_name,mid_name,forth_name,mobile,national_id,email;
    TextView data_of_birth;
    ImageView logo;
    int year,month,day;
    long user_id;

    EditText title;
    Button register;
    EditText grade;
    ImageView image_file,identity_file ;
    LinearLayout equal_layout;

    long cer_id = 0;
    long image_id=0;
    long identity_image=0;
    long family_card_image_id = 0;
    long equal_certificate_id = 0;

        TextView add_certificate;
    LinearLayout profile_layout;
    Button upload_file;
    ImageView profile_image,family_card_image,Attach_the_equation_certificate;
    int file = 1;
    user u;
     String Image_base_64=null;

     List<SpinnerModel> family_values ;
        SpinAdapter family_adapter;
        Spinner family;
        int family_id = 0 ;
        int sex_id = 76 ;
        String education_id = "_none";
        String quran_level =  "_none";
        String master_level =  "_none";
        private RadioGroup radioSexGroup;
        private RadioGroup radiofamily,educationStatge;
        String education_level_id = "_none";

        List<active_awards> active_awardses;

        List<SpinnerModel> country_values ;
        SpinAdapter country_adapter;
        Spinner country;
        int country_id = 0 ;


        List<SpinnerModel> qabila_values ;
        SpinAdapter qabila_adapter;
        Spinner qabila;
        int qabila_id = 0 ;

        List<SpinnerModel> education_place_values ;
        SpinAdapter education_place_adapter;
        Spinner education_place;
        int education_place_id = 0 ;



        private RadioGroup radioQuranGroup;
        private RadioGroup radioMasterGroup;
        String update_profile = null;
        TextView education_level_title;
        LinearLayout education_stage_layout;

        ProgressBar progressBar;
        ScrollView scrollView;

        View view;
        boolean isEditing  = false ;
        long item_id  = 0;

    private OnFragmentInteractionListener mListener;
        private EditText city,neighborhood;


    public static award_registration newInstance(String param1, String param2) {
        award_registration fragment = new award_registration();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public award_registration() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_award_registration, container, false);
        title = (EditText) view.findViewById(R.id.title);
        title.setText("ssss");
        title.setVisibility(View.GONE);
        register = (Button) view.findViewById(R.id.signInButton);
        register.setTypeface(GlobalKeys.getFont(getActivity()));
        grade = (EditText) view.findViewById(R.id.grade);
        image_file = (ImageView) view.findViewById(R.id.image_file);
        profile_image = (ImageView) view.findViewById(R.id.profile_image);
        identity_file = (ImageView) view.findViewById(R.id.idenentity_image);
        profile_layout = (LinearLayout) view.findViewById(R.id.profile_layout);
        radioQuranGroup = (RadioGroup) view.findViewById(R.id.radioQuran);
        radioMasterGroup = (RadioGroup) view.findViewById(R.id.radio_master);
        first_name = (EditText) view.findViewById(R.id.fname);
        mid_name= (EditText) view.findViewById(R.id.middlename);
        last_name = (EditText) view.findViewById(R.id.lastname);
        forth_name = (EditText) view.findViewById(R.id.third_name);
        mobile= (EditText) view.findViewById(R.id.mobilenumer);
        national_id= (EditText) view.findViewById(R.id.national_id);
        data_of_birth = (TextView) view.findViewById(R.id.date_of_birth);
        radioSexGroup = (RadioGroup) view.findViewById(R.id.radioSex);
        radiofamily = (RadioGroup) view.findViewById(R.id.radiofamily);
        email = (EditText) view.findViewById(R.id.email);
        family = (Spinner) view.findViewById(R.id.family);
        education_place = (Spinner) view.findViewById(R.id.education_place);
        country = (Spinner) view.findViewById(R.id.country);
        qabila = (Spinner) view.findViewById(R.id.qabila);
        education_level_title = (TextView) view.findViewById(R.id.education_level_title);
        add_certificate = (TextView) view.findViewById(R.id.add_certificate);
         progressBar  = (ProgressBar) view.findViewById(R.id.loader);
         scrollView  = (ScrollView) view.findViewById(R.id.container);
         city  = view.findViewById(R.id.city);
         neighborhood =  view.findViewById(R.id.state);
        family_card_image = view.findViewById(R.id.family_card_image);
        Attach_the_equation_certificate  = view.findViewById(R.id.Attach_the_equation_certificate);
        equal_layout = view.findViewById(R.id.equal_layout);
        educationStatge = view.findViewById(R.id.radioEducationStage);
        education_stage_layout = view.findViewById(R.id.education_stage_layout);



       // TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);

         index = getArguments().getInt("index");
        getActiveAwards();
        if(index==1){
            //tfawq
            radiofamily.setVisibility(View.VISIBLE);
           // award_type = 182 ;
        }else if(index==2){
            //banen
            radioMasterGroup.setVisibility(View.VISIBLE);
            radioSexGroup.check(R.id.radioMale);
            view.findViewById(R.id.radioFemale).setVisibility(View.GONE);
            sex_id = 76;

           // award_type =155;
        }else if(index==3){
            radioQuranGroup.setVisibility(View.VISIBLE);
            education_level_title.setText(R.string.saved_amount);
            education_stage_layout.setVisibility(View.VISIBLE);
            grade.setVisibility(View.GONE);
            add_certificate.setText(R.string.attach_litter);
            //qur2an
           // award_type = 214;
        }else if(index==4){
            radioMasterGroup.setVisibility(View.VISIBLE);
            radioSexGroup.check(R.id.radioFemale);
            view.findViewById(R.id.radioMale).setVisibility(View.GONE);
            profile_layout.setVisibility(View.GONE);
            sex_id = 77;
            //bant

           // award_type =250;
        }else {
            // editing
            item_id = index;
            get_single_register(item_id);
            isEditing = true;
        }



        update_ui(new user());




        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title.setError(null);
                email.setError(null);
                String _title = title.getText().toString().trim();

                grade.setError(null);
                String _grade = grade.getText().toString().trim();
                first_name.setError(null);
                last_name.setError(null);
                mobile.setError(null);
                national_id.setError(null);
                data_of_birth.setError(null);
                forth_name.setError(null);
                mid_name.setError(null);
                city.setError(null);
                neighborhood.setError(null);

                String fname = first_name.getText().toString().trim();
                String flast = last_name.getText().toString().trim();
                String Mmobile = mobile.getText().toString().trim();
                String Mnational = national_id.getText().toString().trim();
                String date = data_of_birth.getText().toString().trim();
                String _third_name = forth_name.getText().toString().trim();
                String _mid_name = mid_name.getText().toString().trim();
                String _email = email.getText().toString().trim();;
                if(TextUtils.isEmpty(fname)){
                    first_name.setError(getString(R.string.error_field_required));
                }else if(TextUtils.isEmpty(flast)){
                    last_name.setError(getString(R.string.error_field_required));
                }else if(TextUtils.isEmpty(_mid_name)){
                    mid_name.setError(getString(R.string.error_field_required));
                }else if(TextUtils.isEmpty(_third_name)){
                    forth_name.setError(getString(R.string.error_field_required));
                }
                else  if(country_id==0){
                    Toast.makeText(getActivity(),"بالرجاء اختيار البلدة", Toast.LENGTH_SHORT).show();

                }else  if(qabila_id==0){
                    Toast.makeText(getActivity(),"بالرجاء اختيار القبيلة", Toast.LENGTH_SHORT).show();

                }else if(city.getText().toString().trim().length() <= 0){
                    city.setError(getString(R.string.error_field_required));

                }else if(neighborhood.getText().toString().trim().length() <= 0){
                    neighborhood.setError(getString(R.string.error_field_required));

                }else  if(education_place_id==0){
                    Toast.makeText(getActivity(),"بالرجاء اختيار مكان الدراسة", Toast.LENGTH_SHORT).show();

                }
                else if(TextUtils.isEmpty(Mmobile)){
                    mobile.setError(getString(R.string.error_field_required));
                }else if(TextUtils.isEmpty(Mnational)){
                    national_id.setError(getString(R.string.error_field_required));
                }else if(TextUtils.isEmpty(date)){
                    data_of_birth.setError(getString(R.string.error_field_required));
                }else if(TextUtils.isEmpty(_title)){
                    title.setError(getString(R.string.error_field_required));
                }else if(TextUtils.isEmpty(_email)){
                    email.setError(getString(R.string.error_field_required));
                }else if(!utils.isEmailValid(_email)){
                    email.setError(getString(R.string.error_invalid_email));
                }else if(TextUtils.isEmpty(_grade) && index !=3  ){
                    grade.setError(getString(R.string.error_field_required));

                }else if(index !=3 &&!isValidGrade(Double.parseDouble(_grade))){
                    grade.setError("المعدل التراكمى  غير صحيح للتسجيل");
                }else if(cer_id ==0){
                    if(index==3){
                        Toast.makeText(getActivity(),"بالرجاء إرفاق الخطاب", Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(getActivity(), R.string.attach_cert, Toast.LENGTH_SHORT).show();

                    }
                }else if(family_card_image_id==0){
                    Toast.makeText(getActivity(),"بالرجاء إرفاق بطاقة العائلة", Toast.LENGTH_SHORT).show();

                }else if(education_place_id == 106 && equal_certificate_id==0){
                    Toast.makeText(getActivity(),"بالرجاء إرفاق  المعادلة للشهادة", Toast.LENGTH_SHORT).show();

                }else{




                    update_profile = "\n" +
                            "{\n" +
                            "  \"name\": \"ssss\",\n" +
                            "  \"mail\": \"s@s.com\",\n" +
                            "  \"field_first_name\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+fname+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            " \"field_fourth_name\": {" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+_third_name+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_middle_name\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+mid_name.getText().toString().trim()+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_user_image\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"fid\": \""+image_id+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_attach_identity_image\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"fid\": \""+identity_image+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_family_identity\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"fid\": \""+family_card_image_id+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_certificate_equation\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"fid\": \""+equal_certificate_id+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_birth_date\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": {\"date\":\""+data_of_birth.getText().toString()+"\"}\n" +
                            "      }]\n" +
                            "  },\n" +
                            "  \"field_last_name\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+flast+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_mobile_number\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+mobile.getText().toString()+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_city\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+city.getText().toString()+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_neighborhood\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+neighborhood.getText().toString()+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_civil_registry\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+national_id.getText().toString()+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_education_location\": {\n" +
                            "    \"und\": \n" +
                            "      {\n" +
                            "        \"tid\": \""+education_place_id+"\"\n" +
                            "      }\n" +
                            "    \n" +
                            "  },\n"+
                            "  \"field_educational_level\": {\n" +
                            "    \"und\": \n" +
                            "      {\n" +
                            "        \"tid\": \""+education_level_id+"\"\n" +
                            "      }\n" +
                            "    \n" +
                            "  },\n"+
                            "  \"field_education_stage\": {\n" +
                            "    \"und\": \n" +
                            "      {\n" +
                            "        \"tid\": \""+education_id+"\"\n" +
                            "      }\n" +
                            "    \n" +
                            "  },\n" +
                            "  \"field_town\": {\n" +
                            "    \"und\": \n" +
                            "      {\n" +
                            "        \"tid\": \""+country_id+"\"\n" +
                            "      }\n" +
                            "    \n" +
                            "  },\n" +
                            "  \"field_clan\": {\n" +
                            "    \"und\": \n" +
                            "      {\n" +
                            "        \"tid\": \""+qabila_id+"\"\n" +
                            "      }\n" +
                            "    \n" +
                            "  },\n" +
                            "  \"field_number_of_holy_quran_parts\": {\n" +
                            "    \"und\": \n" +
                            "      {\n" +
                            "        \"tid\": \""+quran_level+"\"\n" +
                            "      }\n" +
                            "    \n" +
                            "  },\n" +
                            "  \"field_academic_degree\": {\n" +
                            "    \"und\": \n" +
                            "      {\n" +
                            "        \"tid\": \""+master_level+"\"\n" +
                            "      }\n" +
                            "    \n" +
                            "  },\n" +
                            "  \"field_gender\": {\n" +
                            "    \"und\": \n" +
                            "      {\n" +
                            "        \"value\": \""+sex_id+"\"\n" +
                            "      }\n" +
                            "  },\n" +
                            "  \"field_family\": {\n" +
                            "    \"und\": \n" +
                            "      {\n" +
                            "        \"value\": \""+family_id   +"\"\n" +
                            "      }\n" +
                            "    \n" +
                            "  },\n" +
                            "    \"field_percentage_or_rate\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+grade.getText().toString().trim()+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "    \"field_email_address\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+_email+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  },\n" +
                            "  \"field_award\": {" +
                            "    \"und\":" +
                            "        [\n" +
                            "      {\n" +
                            "        \"nid\": \""+award_type+"\"\n" +
                            "      }\n" +
                            "      ]" +
                            "    \n" +
                            "  },\n" +
                            "  \"title\": \""+title.getText().toString().trim()+"\", " +
                            "  \"type\": \"award_registration\",\n" +
                            "  \"language\": \"und\",\n" +
                            "  \"body\": [],\n" +
                            "  \"field_attach_degree\": { \n"+
                            "    \"und\": \n" +
                            "      [{\n" +
                            "        \"fid\": \""+cer_id+"\"\n" +
                            "      }]\n" +
                            "    \n" +
                            "  },\n" +
                            "  \"field_national_id\": {\n" +
                            "    \"und\": [\n" +
                            "      {\n" +
                            "        \"value\": \""+national_id.getText().toString()+"\"\n" +
                            "      }\n" +
                            "    ]\n" +
                            "  }\n" +
                            "}";

                    if(!isEditing){
                        new HttpRequestTaskRegister().execute(update_profile);

                    }else {
                        new HttpRequestTaskRegisterUpdate().execute(update_profile);

                    }






                }
            }
        });

        image_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                file = 1;
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    GlobalKeys.selectImage(getActivity());

                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        GlobalKeys.selectImage(getActivity());
                    } else {


                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},23
                        );
                        GlobalKeys.selectImage(getActivity());
                    }
                }else{

                    GlobalKeys.selectImage(getActivity());


                }
            }
        });
        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                file = 2;
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    GlobalKeys.selectImage(getActivity());

                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        GlobalKeys.selectImage(getActivity());
                    } else {


                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},23
                        );
                        GlobalKeys.selectImage(getActivity());
                    }
                }else{

                        GlobalKeys.selectImage(getActivity());


                }
            }
        });


        identity_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                file = 3;
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    GlobalKeys.selectImage(getActivity());

                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        GlobalKeys.selectImage(getActivity());
                    } else {


                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},23
                        );

                    }
                }else{
                    GlobalKeys.selectImage(getActivity());

                }

            }
        });

        family_card_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                file = 4;
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    GlobalKeys.selectImage(getActivity());

                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        GlobalKeys.selectImage(getActivity());
                    } else {


                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},23
                        );

                    }
                }else{
                    GlobalKeys.selectImage(getActivity());

                }
            }
        });

        Attach_the_equation_certificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                file = 5;
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    GlobalKeys.selectImage(getActivity());

                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        GlobalKeys.selectImage(getActivity());
                    } else {


                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},23
                        );

                    }
                }else{
                    GlobalKeys.selectImage(getActivity());

                }
            }
        });

        SpinnerModel spinnerModel = new SpinnerModel();
        family_values = new ArrayList<>();
        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(0);
        spinnerModel.setValue("-" + getString(R.string.family) + "-");
        family_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(82);
        spinnerModel.setValue(getString(R.string.ajalan));
        family_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(83);
        spinnerModel.setValue(getString(R.string.el3ed));
        family_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(84);
        spinnerModel.setValue(getString(R.string.eltwel));
        family_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(85);
        spinnerModel.setValue(getString(R.string.brothers_of_ebn_fahd));
        family_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(86);
        spinnerModel.setValue(getString(R.string.sons_of_abdel_azez));
        family_values.add(spinnerModel);

        family = (Spinner) view.findViewById(R.id.family);
        family_adapter = new SpinAdapter(getActivity(),
                android.R.layout.simple_spinner_item, family_values);
        family_adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        family.setAdapter(family_adapter);


        radioSexGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioMale) {
                    sex_id = 76;
                    profile_layout.setVisibility(View.VISIBLE);
                } else {
                    sex_id = 77;
                    profile_layout.setVisibility(View.GONE);
                }
            }
        });



        educationStatge.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId==R.id.primary){
                    education_id = 107+"";
                }else if (checkedId==R.id.middle){
                    education_id = 108+"";
                }else if (checkedId==R.id.high){
                    education_id = 109+"";
                }else if (checkedId==R.id.diploma){
                    education_id = 110+"";
                }else if (checkedId==R.id.bachelor){
                    education_id = 111+"";
                }else if (checkedId==R.id.masters){
                    education_id = 112+"";
                }else if (checkedId==R.id.phD){
                    education_id = 113+"";
                }

            }
        });

        radiofamily.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                if(checkedId==R.id.first_class){
                                    education_level_id = 79+"";
                                   }else if (checkedId==R.id.second_class){
                                    education_level_id = 80+"";
                                    }else if (checkedId==R.id.third_class){
                                    education_level_id = 81+"";
                                    }else if (checkedId==R.id.high_first){
                                    education_level_id = 87+"";
                                    }else if (checkedId==R.id.high_second){
                                    education_level_id = 88+"";
                                    }else if (checkedId==R.id.high_third){
                                    education_level_id = 89+"";
                                    }else if (checkedId==R.id.undergraduate){
                                        education_level_id = 90+"";
                                   }else if (checkedId==R.id.graduated){
                                    education_level_id = 91+"";
                                    }

              }
        });



        country_values = new ArrayList<>();
        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(0);
        spinnerModel.setValue("-" + getString(R.string.country) + "-");
        country_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(102);
        spinnerModel.setValue(getString(R.string.ragba));
        country_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(103);
        spinnerModel.setValue(getString(R.string.elbara));
        country_values.add(spinnerModel);


        country_adapter = new SpinAdapter(getActivity(),
                android.R.layout.simple_spinner_item, country_values);
        country_adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        country.setAdapter(country_adapter);

        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(position!=0){
                    country_id = country_values.get(position).getKey();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        qabila_values = new ArrayList<>();
        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(0);
        spinnerModel.setValue("-" + getString(R.string.qablia) + "-");
        qabila_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(104);
        spinnerModel.setValue(getString(R.string.hazel));
        qabila_values.add(spinnerModel);


        qabila_adapter = new SpinAdapter(getActivity(),
                android.R.layout.simple_spinner_item, qabila_values);
        qabila_adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        qabila.setAdapter(qabila_adapter);

        qabila.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(position!=0){
                    qabila_id = qabila_values.get(position).getKey();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        education_place_values = new ArrayList<>();
        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(0);
        spinnerModel.setValue("-" + getString(R.string.education_place) + "-");
        education_place_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(105);
        spinnerModel.setValue(getString(R.string.at_ksa));
        education_place_values.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(106);
        spinnerModel.setValue(getString(R.string.out_ksa));
        education_place_values.add(spinnerModel);


        education_place_adapter = new SpinAdapter(getActivity(),
                android.R.layout.simple_spinner_item, education_place_values);
        education_place_adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        education_place.setAdapter(education_place_adapter);

        education_place.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(position!=0){
                    education_place_id = education_place_values.get(position).getKey();
                    if(position==1){
                        equal_layout.setVisibility(View.GONE);
                    }else if(position==2){
                        equal_layout.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        PrefManager prefManager = new PrefManager(getActivity());
        user_id = prefManager.getUserId();
        /*new HttpRequestTaskUser().execute(user_id);*/
        return  view;
    }

        private void getItem(long item_id) {

        }

        private boolean isValidGrade(double grade) {
        if(index==1){
            if((education_level_id.equals("79") ||  education_level_id.equals("80")) &&  grade >= 90){
                return  true;
            }else if(education_level_id.equals("81") && grade >= 87.5){
                return  true;
            }else if((education_level_id.equals("87") ||  education_level_id.equals("88")) &&  grade >= 90){
                return  true;
            }else if(education_level_id.equals("89") && grade >= 87.5){
                return  true;
            }else  if(education_level_id.equals("90") && grade >= 80){
                return true;
            }else if(education_level_id.equals("91") && grade >= 3.25){
                return  true;
            }else{
                return false;
            }
        }else{
            return  true;
        }


        }

        // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if(file==1){
                Image_base_64 = GlobalKeys.ImageResult(requestCode,data,image_file,getActivity());

            }else if(file==2){
                Image_base_64 = GlobalKeys.ImageResult(requestCode,data,profile_image,getActivity());

            }else if(file==3){
                Image_base_64 = GlobalKeys.ImageResult(requestCode,data,identity_file,getActivity());

            }else  if(file==4){
                Image_base_64 = GlobalKeys.ImageResult(requestCode,data,family_card_image,getActivity());

            }else  if(file==5){
                Image_base_64 = GlobalKeys.ImageResult(requestCode,data,Attach_the_equation_certificate,getActivity());

            }
            if(Image_base_64!=null){
                JSONObject jsonObject = new JSONObject();
                Long tsLong = System.currentTimeMillis()/1000;
                String ts = tsLong.toString();
                try {
                    jsonObject.put("file",Image_base_64);
                    jsonObject.put("filename",ts+".jpg");
                    jsonObject.put("filename", "sites/default/files/"+ts+".jpg");
                    String main = jsonObject.toString();
                    //upload_file(loginService, main);
                    new HttpRequestTask().execute(main);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else{
                Toast.makeText(getActivity(), R.string.select_cert, Toast.LENGTH_SHORT).show();
            }
        }

        if(resultCode==3000){
            getActivity().onBackPressed();
        }
    }





        String  Result;
        private class HttpRequestTask extends AsyncTask<String, Void ,Void> {
            public ProgressDialog progress;
            @Override
            protected void onPreExecute() {
                progress = new ProgressDialog(getActivity());
                progress.setMessage(getString(R.string.loading));
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            protected Void doInBackground(String... params) {



                try {

                    String InsertTransactionResult = null;
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpParams myParams = new BasicHttpParams();
                    HttpConnectionParams.setConnectionTimeout(myParams, 100000);
                    HttpConnectionParams.setSoTimeout(myParams, 100000);

                    try {

                        PrefManager prefManager = new PrefManager(getActivity());
                        String mUsername = prefManager.getUsername();
                        String mPassword = prefManager.getPassword();
                        HttpPost httppost = new HttpPost(GlobalKeys.GLBURL+"test/file.json");
                        httppost.setHeader("Content-type", "application/json");

                        String base64EncodedCredentials = "Basic " + Base64.encodeToString(
                                (mUsername + ":" + mPassword).getBytes(),
                                Base64.NO_WRAP);
                        httppost.setHeader("Authorization", base64EncodedCredentials);

                        StringEntity se = new StringEntity(params[0]);

                        se.setContentEncoding(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE,
                                "application/json"));
                        httppost.setEntity(se);

                        HttpResponse response = httpclient.execute(httppost);

                        Result = EntityUtils
                                .toString(response.getEntity());

                        Log.d("Response" , ""+Result);

                    } catch (ClientProtocolException e) {
                            e.printStackTrace();
                    } catch (IOException e) {
                    }

                } catch (Exception e) {
                    Log.e("MainActivity", e.getMessage(), e);
                }
                return null;



            }


            @Override
            protected void onPostExecute(Void aVoid) {
                progress.dismiss();
                if(Result.contains("fid")){
                    try {
                        JSONObject jsonObject = new JSONObject(Result);
                        if(file==1){
                            cer_id = jsonObject.getLong("fid");
                            Toast.makeText(getActivity(), R.string.cert_done, Toast.LENGTH_SHORT).show();

                        }else if(file==2){
                            image_id = jsonObject.getLong("fid");
                            Toast.makeText(getActivity(), R.string.image_uploaded, Toast.LENGTH_SHORT).show();
                        }else if(file==3){
                            identity_image = jsonObject.getLong("fid");
                            Toast.makeText(getActivity(), R.string.identity_done, Toast.LENGTH_SHORT).show();
                        }else if(file==4){
                            family_card_image_id = jsonObject.getLong("fid");
                            Toast.makeText(getActivity(), R.string.uploaded_successfuly, Toast.LENGTH_SHORT).show();
                        }else if(file==5){
                            equal_certificate_id= jsonObject.getLong("fid");
                            Toast.makeText(getActivity(),R.string.uploaded_successfuly, Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(getActivity(), R.string.try_again, Toast.LENGTH_SHORT).show();
                }

            }
        }



        String  Result1;
        private class HttpRequestTaskRegister extends AsyncTask<String, Void ,Void> {
            public ProgressDialog progress;
            @Override
            protected void onPreExecute() {
                progress = new ProgressDialog(getActivity());
                progress.setMessage(getString(R.string.loading));
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            protected Void doInBackground(String... params) {



                try {

                    String InsertTransactionResult = null;
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpParams myParams = new BasicHttpParams();
                    HttpConnectionParams.setConnectionTimeout(myParams, 100000);
                    HttpConnectionParams.setSoTimeout(myParams, 100000);

                    try {

                        PrefManager prefManager = new PrefManager(getActivity());
                        String mUsername = prefManager.getUsername();
                        String mPassword = prefManager.getPassword();

                        HttpPost httppost = new HttpPost(GlobalKeys.GLBURL+"test/node.json");
                        httppost.setHeader("Content-type", "application/json");

                        String base64EncodedCredentials = "Basic " + Base64.encodeToString(
                                (mUsername + ":" + mPassword).getBytes(),
                                Base64.NO_WRAP);
                        httppost.setHeader("Authorization", base64EncodedCredentials);

                        StringEntity se = new StringEntity(params[0], HTTP.UTF_8);
                        se.setContentEncoding(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE,
                                "application/json"));
                        httppost.setEntity(se);

                        HttpResponse response = httpclient.execute(httppost);
                        Result = null;
                        Result = EntityUtils
                                .toString(response.getEntity());

                        Log.d("Response" , ""+Result);

                    } catch (ClientProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                    }

                } catch (Exception e) {
                    Log.e("MainActivity", e.getMessage(), e);
                }
                return null;



            }


            @Override
            protected void onPostExecute(Void aVoid) {
                progress.dismiss();
                if(Result != null && Result.matches(".*\\d+.*") && Result.contains("uri")){
                    ThanksFragment filterSrFragment = new ThanksFragment();
                    filterSrFragment.setTargetFragment(award_registration.this, 2000);
                    Bundle bundle = new Bundle();
                    bundle.putString("title",getString(R.string.register_done));
                    filterSrFragment.setArguments(bundle);
                    filterSrFragment.show(getActivity().getSupportFragmentManager(), "dialog");
                        /*Toast.makeText(getActivity(), , Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();*/

                }else{

                    Toast.makeText(getActivity(), "خطاء فى التسجيل حاول مرة اخرى", Toast.LENGTH_SHORT).show();
                }

            }
        }
        private class HttpRequestTaskRegisterUpdate extends AsyncTask<String, Void ,Void> {
            public ProgressDialog progress;
            @Override
            protected void onPreExecute() {
                progress = new ProgressDialog(getActivity());
                progress.setMessage(getString(R.string.loading));
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            protected Void doInBackground(String... params) {



                try {

                    String InsertTransactionResult = null;
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpParams myParams = new BasicHttpParams();
                    HttpConnectionParams.setConnectionTimeout(myParams, 100000);
                    HttpConnectionParams.setSoTimeout(myParams, 100000);

                    try {

                        PrefManager prefManager = new PrefManager(getActivity());
                        String mUsername = prefManager.getUsername();
                        String mPassword = prefManager.getPassword();

                        HttpPut httppost = new HttpPut(GlobalKeys.GLBURL+"test/node/"+item_id+".json");
                        httppost.setHeader("Content-type", "application/json");

                        String base64EncodedCredentials = "Basic " + Base64.encodeToString(
                                (mUsername + ":" + mPassword).getBytes(),
                                Base64.NO_WRAP);
                        httppost.setHeader("Authorization", base64EncodedCredentials);

                        StringEntity se = new StringEntity(params[0], HTTP.UTF_8);
                        se.setContentEncoding(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE,
                                "application/json"));
                        httppost.setEntity(se);

                        HttpResponse response = httpclient.execute(httppost);
                        Result = null;
                        Result = EntityUtils
                                .toString(response.getEntity());

                        Log.d("Response" , ""+Result);

                    } catch (ClientProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                    }

                } catch (Exception e) {
                    Log.e("MainActivity", e.getMessage(), e);
                }
                return null;



            }


            @Override
            protected void onPostExecute(Void aVoid) {
                progress.dismiss();
                if(Result != null && Result.matches(".*\\d+.*") && Result.contains("uri")){
                    ThanksFragment filterSrFragment = new ThanksFragment();
                    filterSrFragment.setTargetFragment(award_registration.this, 2000);
                    Bundle bundle = new Bundle();
                    bundle.putString("title",getString(R.string.edit_done_success));
                    filterSrFragment.setArguments(bundle);
                    filterSrFragment.show(getActivity().getSupportFragmentManager(), "dialog");
                    /*Toast.makeText(getActivity(), R.string.edit_done_success, Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();*/

                }else{

                    Toast.makeText(getActivity(), "خطاء فى التسجيل حاول مرة اخرى", Toast.LENGTH_SHORT).show();
                }

            }
        }


        private void update_ui(user u) {
            first_name.setText(u.getFirst_name());
            last_name.setText(u.getLast_name());
            mid_name.setText(u.getMed_name());
            forth_name.setText(u.getForth_name());
            mobile.setText(u.getMobile());
            national_id.setText(u.getNational_id());



            String img= GlobalKeys.GLBURL+u.getPic_url();
            Picasso.with(getActivity())
                    .load(img)
                    .placeholder(R.drawable.plussymbol)
                    .into(profile_image, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });





            family.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if(position!=0){
                        family_id = family_values.get(position).getKey();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            family_id = u.getFamily();



            //date view
            final Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);


            final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, monthOfYear);
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateLabel(calendar);
                }

            };

            data_of_birth.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new DatePickerDialog(getActivity(), date, calendar
                            .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });



            radioMasterGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if(checkedId == R.id.master)
                        master_level  = 100+"";
                    else
                        master_level  = 101+"";

                }
            });

            radioQuranGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if(checkedId == R.id.fife_part)
                        quran_level  = 96+"";
                    else if(checkedId == R.id.ten_parts)
                        quran_level  = 97+"";
                    else if(checkedId == R.id.twenty_part)
                        quran_level  = 98+"";
                    else if(checkedId == R.id.all_parts)
                        quran_level  = 99+"";


                }
            });








        }
        private void updateLabel(Calendar ca) {

            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd",Locale.US);

            data_of_birth.setText(format1.format(ca.getTime()));
        }


        public  void getActiveAwards(){

                if(index==1 ){

                    //tfawq
                    award_type = 182;
                }else if(index==2){
                    //banen
                    // award_type =155;

                    award_type = 251;
                }else if(index==3 ){

                    //qur2an
                    award_type = 214;
                    // award_type = active_awardses.get(i).getNid();
                }else if(index==4 ){

                    //bant
                    // award_type =250;
                    award_type = 250;
                }

           /* Call<List<active_awards>> call = ApiClient.getApiInterface().getActiveAwards();
            call.enqueue(new Callback<List<active_awards>>() {
                @Override
                public void onResponse(Call<List<active_awards>> call, Response<List<active_awards>> response) {
                    progressBar.setVisibility(View.GONE);
                    if(response.isSuccessful() && response.body().size() > 0){
                        scrollView.setVisibility(View.VISIBLE);
                        active_awardses = response.body();


                        for (int i = 0 ; i <=3; i++){
                            if(index==1 && active_awardses.get(i).getAward_type_id() == 157){

                                //tfawq
                                 award_type = 182;
                            }else if(index==2 && active_awardses.get(i).getAward_type_id() == 155){
                                //banen
                                // award_type =155;

                                award_type = 251;
                            }else if(index==3 && active_awardses.get(i).getAward_type_id() == 156){

                                //qur2an
                                 award_type = 214;
                               // award_type = active_awardses.get(i).getNid();
                            }else if(index==4 && active_awardses.get(i).getAward_type_id() == 154){

                                //bant
                                // award_type =250;
                                award_type = 250;
                            }
                        }


                    }else{
                        Toast.makeText(getContext(), getString(R.string.try_again), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<List<active_awards>> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getContext(), getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                }
            });*/
        }


        @Override
        public void onPrepareOptionsMenu(Menu menu) {
            MenuItem item = menu.findItem(R.id.action_search);
            item.setVisible(false);
        }

        public  void get_single_register(final long  id){

            PrefManager prefManager  = new PrefManager(getActivity());
            String base64EncodedCredentials = "Basic " + Base64.encodeToString(
                    (prefManager.getUsername() + ":" + prefManager.getPassword()).getBytes(),
                    Base64.NO_WRAP);
            Call<String> call = ApiClient.getApiInterface().getSingleAward(GlobalKeys.get_lang(getActivity()),id,"application/json", base64EncodedCredentials);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    progressBar.setVisibility(View.GONE);
                    if(response.isSuccessful() && response.body() != null){
                        scrollView.setVisibility(View.VISIBLE);
                        SingleRegisterModel singleRegisterModel = new SingleRegisterModel();
                        singleRegisterModel.setId(id);
                        try {
                            JSONObject object = new JSONObject(response.body());
                            singleRegisterModel.setAward_type(object.getJSONObject("field_award").getJSONArray("und").getJSONObject(0).getInt("nid"));
                            singleRegisterModel.setFirstname(object.getJSONObject("field_first_name").getJSONArray("und").getJSONObject(0).getString("value"));
                            singleRegisterModel.midLastname(object.getJSONObject("field_middle_name").getJSONArray("und").getJSONObject(0).getString("value"));
                            singleRegisterModel.setThirdname(object.getJSONObject("field_last_name").getJSONArray("und").getJSONObject(0).getString("value"));
                            singleRegisterModel.setFourthname(object.getJSONObject("field_fourth_name").getJSONArray("und").getJSONObject(0).getString("value"));
                            singleRegisterModel.setCity(object.getJSONObject("field_city").getJSONArray("und").getJSONObject(0).getString("value"));
                            singleRegisterModel.setState(object.getJSONObject("field_neighborhood").getJSONArray("und").getJSONObject(0).getString("value"));
                            singleRegisterModel.setNational_id(object.getJSONObject("field_national_id").getJSONArray("und").getJSONObject(0).getString("value"));
                            singleRegisterModel.setCountry_id(object.getJSONObject("field_town").getJSONArray("und").getJSONObject(0).getInt("tid"));
                            singleRegisterModel.setQablia_id(object.getJSONObject("field_clan").getJSONArray("und").getJSONObject(0).getInt("tid"));
                            singleRegisterModel.setEducation_place(object.getJSONObject("field_education_location").getJSONArray("und").getJSONObject(0).getInt("tid"));
                            singleRegisterModel.setFamily_id(object.getJSONObject("field_family").getJSONArray("und").getJSONObject(0).getInt("tid"));
                            singleRegisterModel.setBirthDay(object.getJSONObject("field_birth_date").getJSONArray("und").getJSONObject(0).getString("value"));
                            singleRegisterModel.setSex(object.getJSONObject("field_gender").getJSONArray("und").getJSONObject(0).getInt("tid"));
                            singleRegisterModel.setMobile(object.getJSONObject("field_mobile_number").getJSONArray("und").getJSONObject(0).getString("value"));
                            singleRegisterModel.setEmail(object.getJSONObject("field_email_address").getJSONArray("und").getJSONObject(0).getString("value"));

                            singleRegisterModel.setGrade(null);
                            Object field_percentage_or_rate  = object.get("field_percentage_or_rate");
                            if(field_percentage_or_rate instanceof JSONObject){
                                singleRegisterModel.setGrade(object.getJSONObject("field_percentage_or_rate").getJSONArray("und").getJSONObject(0).getString("value"));
                            }


                            singleRegisterModel.setQuean_parts(0);
                            Object field_number_of_holy_quran_parts  = object.get("field_number_of_holy_quran_parts");
                            if(field_number_of_holy_quran_parts instanceof JSONObject){

                                singleRegisterModel.setQuean_parts(object.getJSONObject("field_number_of_holy_quran_parts").getJSONArray("und").getJSONObject(0).getInt("tid"));
                                quran_level =  singleRegisterModel.getQuean_parts()+"";
                            }


                            singleRegisterModel.setAcademic(0);
                            Object field_academic_degree  = object.get("field_academic_degree");
                            if(field_academic_degree instanceof JSONObject){

                                singleRegisterModel.setAcademic(object.getJSONObject("field_academic_degree").getJSONArray("und").getJSONObject(0).getInt("tid"));
                                master_level =  singleRegisterModel.getAcademic()+"";
                            }



                            singleRegisterModel.setEducation_level(0);

                            Object field_educational_level  = object.get("field_educational_level");
                            if(field_educational_level instanceof JSONObject){
                                singleRegisterModel.setEducation_level(object.getJSONObject("field_educational_level").getJSONArray("und").getJSONObject(0).getInt("tid"));
                            }

                            Object field_education_stage  = object.get("field_education_stage");
                            if(field_education_stage instanceof JSONObject){
                                singleRegisterModel.setEducation_stage(object.getJSONObject("field_education_stage").getJSONArray("und").getJSONObject(0).getInt("tid"));
                                education_id = singleRegisterModel.getEducation_stage()+"";

                            }else {
                                singleRegisterModel.setEducation_stage(0);
                            }





                            Object field_attach_degree  = object.get("field_attach_degree");
                            if(field_attach_degree instanceof JSONObject){
                                SingleRegisterModel.image image = new SingleRegisterModel.image();
                                image.id =  object.getJSONObject("field_attach_degree").getJSONArray("und").getJSONObject(0).getInt("fid");
                                image.url = object.getJSONObject("field_attach_degree").getJSONArray("und").getJSONObject(0).getString("filename");
                                singleRegisterModel.setAttach_degree(image);
                            }else {
                                singleRegisterModel.setAttach_degree(null);

                            }

                            Object field_user_image  = object.get("field_user_image");
                            if(field_user_image instanceof JSONObject){
                                SingleRegisterModel.image image = new SingleRegisterModel.image();
                                image.id =  object.getJSONObject("field_user_image").getJSONArray("und").getJSONObject(0).getInt("fid");
                                image.url = object.getJSONObject("field_user_image").getJSONArray("und").getJSONObject(0).getString("filename");
                                singleRegisterModel.setUser_image(image);
                            }else {
                                singleRegisterModel.setUser_image(null);

                            }


                            Object field_attach_identity_image  = object.get("field_attach_identity_image");
                            if(field_attach_identity_image instanceof JSONObject){
                                SingleRegisterModel.image image = new SingleRegisterModel.image();
                                image.id =  object.getJSONObject("field_attach_identity_image").getJSONArray("und").getJSONObject(0).getInt("fid");
                                image.url = object.getJSONObject("field_attach_identity_image").getJSONArray("und").getJSONObject(0).getString("filename");
                                singleRegisterModel.setIdenity_image(image);
                            }else {
                                singleRegisterModel.setIdenity_image(null);

                            }

                            Object family_card_image_id  = object.get("field_family_identity");
                            if(family_card_image_id instanceof JSONObject){
                                SingleRegisterModel.image image = new SingleRegisterModel.image();
                                image.id =  object.getJSONObject("field_family_identity").getJSONArray("und").getJSONObject(0).getInt("fid");
                                image.url = object.getJSONObject("field_family_identity").getJSONArray("und").getJSONObject(0).getString("filename");
                                singleRegisterModel.setFamilyCard(image);
                            }else {
                                singleRegisterModel.setFamilyCard(null);

                            }

                            Object equal_certificate_id  = object.get("field_certificate_equation");
                            if(equal_certificate_id instanceof JSONObject){
                                SingleRegisterModel.image image = new SingleRegisterModel.image();
                                image.id =  object.getJSONObject("field_certificate_equation").getJSONArray("und").getJSONObject(0).getInt("fid");
                                image.url = object.getJSONObject("field_certificate_equation").getJSONArray("und").getJSONObject(0).getString("filename");
                                singleRegisterModel.setAttach_degree_equals(image);
                            }else {
                                singleRegisterModel.setAttach_degree_equals(null);

                            }










                            if(singleRegisterModel.getAward_type()==182){
                                index = 1;
                            }else if(singleRegisterModel.getAward_type()==251){
                                index = 2;
                            }else if(singleRegisterModel.getAward_type()==214){
                                index = 3;
                            }else if(singleRegisterModel.getAward_type()==250){
                                index = 4;
                            }

                            updateEditUI(singleRegisterModel);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }





                        /*for (int i = 0 ; i <=3; i++){
                            if(index==1 && active_awardses.get(i).getAward_type_id() == 157){

                                //tfawq
                                 award_type = 182;
                            }else if(index==2 && active_awardses.get(i).getAward_type_id() == 155){
                                //banen
                                // award_type =155;

                                award_type = 251;
                            }else if(index==3 && active_awardses.get(i).getAward_type_id() == 156){

                                //qur2an
                                 award_type = 214;
                               // award_type = active_awardses.get(i).getNid();
                            }else if(index==4 && active_awardses.get(i).getAward_type_id() == 154){

                                //bant
                                // award_type =250;
                                award_type = 250;
                            }
                        }*/


                    }else{
                        Toast.makeText(getContext(), getString(R.string.try_again), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getContext(), getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                }
            });
        }

        private void updateEditUI(SingleRegisterModel single) {

            first_name.setText(single.getFirstname());
            last_name.setText(single.getThirdname());
            mid_name.setText(single.getMidname());
            forth_name.setText(single.getFourthname());
            city.setText(single.getCity());
            neighborhood.setText(single.getState());
            national_id.setText(single.getNational_id());
            email.setText(single.getEmail());
            grade.setText(single.getGrade());
            mobile.setText(single.getMobile());

            if(country_values.get(1).getKey()==single.getCountry_id()){
                country.setSelection(1);
            }else{
                country.setSelection(2);
            }
            country_id  =  single.getCountry_id();

            for (int  i =  0;  i < family_values.size() ; i++){
                if(family_values.get(i).getKey()==single.getFamily_id()){
                    family.setSelection(i);
                }
            }
            family_id  = single.getFamily_id();


            if(qabila_values.get(1).getKey()==single.getQablia_id()){
                qabila.setSelection(1);
            }

            qabila_id  = single.getQablia_id();

            if(education_place_values.get(1).getKey()==single.getEducation_place()){
                education_place.setSelection(1);
            }else {
                education_place.setSelection(2);
            }

            education_place_id = single.getEducation_place();

            String birthdate =  single.getBirthDay().split(" ")[0];
            data_of_birth.setText(birthdate);

            if(single.getSex()==76){
                radioSexGroup.check(R.id.radioMale);
            }else {
                radioSexGroup.check(R.id.radioFemale);
            }

            sex_id = single.getSex();


            if(single.getEducation_level()==79){
                radiofamily.check(R.id.first_class);
            }else if(single.getEducation_level()==80){
                radiofamily.check(R.id.second_class);

            }else if(single.getEducation_level()==81){
                radiofamily.check(R.id.third_class);

            }else if(single.getEducation_level()==87){
                radiofamily.check(R.id.high_first);

            }else if(single.getEducation_level()==88){
                radiofamily.check(R.id.high_second);

            }else if(single.getEducation_level()==89){
                radiofamily.check(R.id.high_third);

            }else if(single.getEducation_level()==90){
                radiofamily.check(R.id.undergraduate);

            }else if(single.getEducation_level()==91){
                radiofamily.check(R.id.graduated);

            }


            if(single.getEducation_level()!=0){
                education_level_id = single.getEducation_level()+"";
            }

            if(single.getAttach_degree()!=null){
                cer_id = single.getAttach_degree().id;
                Picasso.with(getActivity()).load("http://ajlanbrosaward.com/ajlanbrosaward/sites/default/files/"+single.getAttach_degree().url).into(image_file);
            }



            if(single.getUser_image()!=null){
                image_id = single.getUser_image().id;
                Picasso.with(getActivity()).load("http://ajlanbrosaward.com/ajlanbrosaward/sites/default/files/"+single.getUser_image().url).into(profile_image);
            }


            if(single.getIdenity_image()!=null){
                identity_image = single.getIdenity_image().id;
                Picasso.with(getActivity()).load("http://ajlanbrosaward.com/ajlanbrosaward/sites/default/files/"+single.getIdenity_image().url).into(identity_file);
            }


            if(single.getFamilyCard()!=null){
                family_card_image_id = single.getAttach_degree().id;
                Picasso.with(getActivity()).load("http://ajlanbrosaward.com/ajlanbrosaward/sites/default/files/"+single.getFamilyCard().url).into(family_card_image);
            }

            if(single.getAttach_degree_equals()!=null){
                equal_certificate_id = single.getAttach_degree_equals().id;
                Picasso.with(getActivity()).load("http://ajlanbrosaward.com/ajlanbrosaward/sites/default/files/"+single.getAttach_degree_equals().url).into(Attach_the_equation_certificate);
            }


            award_type = single.getAward_type();


            if(single.getQuean_parts()==96){
                radioQuranGroup.check(R.id.fife_part);
            }else if(single.getQuean_parts()==97){
                radioQuranGroup.check(R.id.ten_parts);

            }else if(single.getQuean_parts()==98){
                radioQuranGroup.check(R.id.twenty_part);

            }else if(single.getQuean_parts()==99){
                radioQuranGroup.check(R.id.all_parts);

            }



            if(single.getAcademic()==100){
                radioMasterGroup.check(R.id.master);
            }else if(single.getAcademic()==101){
                radioMasterGroup.check(R.id.phd);

            }



            if(single.getEducation_stage()==107){
                educationStatge.check(R.id.primary);
            }else if(single.getEducation_stage()==108){
                educationStatge.check(R.id.middle);

            }else if(single.getEducation_stage()==109){
                educationStatge.check(R.id.high);

            }else if(single.getEducation_stage()==110){
                educationStatge.check(R.id.diploma);

            }else if(single.getEducation_stage()==111){
                educationStatge.check(R.id.bachelor);

            }else if(single.getEducation_stage()==112){
                educationStatge.check(R.id.masters);

            }else if(single.getEducation_stage()==113){
                educationStatge.check(R.id.phD);

            }















            if(index==1){
                //tfawq
                radiofamily.setVisibility(View.VISIBLE);
                // award_type = 182 ;
            }else if(index==2){
                //banen
                radioMasterGroup.setVisibility(View.VISIBLE);
                radioSexGroup.check(R.id.radioMale);
                view.findViewById(R.id.radioFemale).setVisibility(View.GONE);
                sex_id = 76;

                // award_type =155;
            }else if(index==3){
                radioQuranGroup.setVisibility(View.VISIBLE);
                education_level_title.setText(R.string.saved_amount);
                education_stage_layout.setVisibility(View.VISIBLE);
                grade.setVisibility(View.GONE);
                add_certificate.setText(R.string.attach_litter);
                //qur2an
                // award_type = 214;
            }else if(index==4){
                radioMasterGroup.setVisibility(View.VISIBLE);
                radioSexGroup.check(R.id.radioFemale);
                view.findViewById(R.id.radioMale).setVisibility(View.GONE);
                profile_layout.setVisibility(View.GONE);
                sex_id = 77;
                //bant

                // award_type =250;
            }

        }


    }
