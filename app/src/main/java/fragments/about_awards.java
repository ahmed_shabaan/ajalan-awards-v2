package fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ajlanawards.mivor.com.ajlanawards.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link about_awards.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link about_awards#newInstance} factory method to
 * create an instance of this fragment.
 */
public class about_awards extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public about_awards() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment about_awards.
     */
    // TODO: Rename and change types and number of parameters
    public static about_awards newInstance(String param1, String param2) {
        about_awards fragment = new about_awards();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }
    FragmentTransaction transaction;
    FragmentManager manager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  =  inflater.inflate(R.layout.fragment_about_awards, container, false);
        manager = getActivity().getSupportFragmentManager();
        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
        main_title.setText(getString(R.string.about_award));


        RelativeLayout award_idea = (RelativeLayout) view.findViewById(R.id.award_idea);
        award_idea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transaction = manager.beginTransaction();
                about_awards_details about_awards_details = new about_awards_details();
                Bundle b  = new Bundle();
                b.putInt("index",1);
                about_awards_details.setArguments(b);
                transaction.replace(R.id.myfrgmentlayout,about_awards_details , "about_awards_details");
                transaction.addToBackStack("about_awards_details");
                transaction.commit();
            }
        });

        RelativeLayout award_goals= (RelativeLayout) view.findViewById(R.id.award_goals);
        award_goals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transaction = manager.beginTransaction();
                about_awards_details about_awards_details = new about_awards_details();
                Bundle b  = new Bundle();
                b.putInt("index",2);
                about_awards_details.setArguments(b);
                transaction.replace(R.id.myfrgmentlayout,about_awards_details , "about_awards_details");
                transaction.addToBackStack("about_awards_details");
                transaction.commit();
            }
        });

        RelativeLayout award_commmitee= (RelativeLayout) view.findViewById(R.id.award_commmitee);
        award_commmitee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transaction = manager.beginTransaction();
                prizeComunity about_awards_details = new prizeComunity();
                Bundle b  = new Bundle();
                b.putInt("index",5);
                about_awards_details.setArguments(b);
                transaction.replace(R.id.myfrgmentlayout,about_awards_details , "about_awards_details");
                transaction.addToBackStack("about_awards_details");
                transaction.commit();
            }
        });

        RelativeLayout award_mechanism= (RelativeLayout) view.findViewById(R.id.award_mechanism);
        award_mechanism.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transaction = manager.beginTransaction();
                about_awards_details about_awards_details = new about_awards_details();
                Bundle b  = new Bundle();
                b.putInt("index",5);
                about_awards_details.setArguments(b);
                transaction.replace(R.id.myfrgmentlayout,about_awards_details , "about_awards_details");
                transaction.addToBackStack("about_awards_details");
                transaction.commit();
            }
        });

        RelativeLayout award_vision= (RelativeLayout) view.findViewById(R.id.award_vision);
        award_vision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transaction = manager.beginTransaction();
                about_awards_details about_awards_details = new about_awards_details();
                Bundle b  = new Bundle();
                b.putInt("index",3);
                about_awards_details.setArguments(b);
                transaction.replace(R.id.myfrgmentlayout,about_awards_details , "about_awards_details");
                transaction.addToBackStack("about_awards_details");
                transaction.commit();
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }
}
