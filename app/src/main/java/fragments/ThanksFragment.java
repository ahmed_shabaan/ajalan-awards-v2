package fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ajlanawards.mivor.com.ajlanawards.R;

/**
 * Created by Ahmed shaban on 1/10/2018.
 */

public class ThanksFragment extends android.support.v4.app.DialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.thanks_item, container);
        TextView  textViewTitle= view.findViewById(R.id.title);
        textViewTitle.setText(getArguments().getString("title"));

        view.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                notifyToTarget(3000);
            }
        });


        return view;
    }


    private void notifyToTarget(int code) {
        android.support.v4.app.Fragment targetFragment = getTargetFragment();
        if (targetFragment != null) {
            Intent data  = new Intent();
            targetFragment.onActivityResult(getTargetRequestCode(), code, data);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }





}