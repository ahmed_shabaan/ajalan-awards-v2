package fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Accessors.GlobalKeys;
import ApiClient.ApiClient;
import adapters.RoundAdapterV1;
import ajlanawards.mivor.com.ajlanawards.R;
import models.*;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link rounds.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link rounds#newInstance} factory method to
 * create an instance of this fragment.
 */
public class rounds extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    FragmentManager manager;
    FragmentTransaction transaction;
    ProgressBar loader;

    List<round> roundList = new ArrayList<>();
    ListView rounds;
    RoundAdapterV1 roundAdapter;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment rounds.
     */
    // TODO: Rename and change types and number of parameters
    public static rounds newInstance(String param1, String param2) {
        rounds fragment = new rounds();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public rounds() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_rounds, container, false);
        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
        main_title.setText(R.string.winners);

        loader = (ProgressBar) view.findViewById(R.id.loader);
        rounds = (ListView) view.findViewById(R.id.list_round_videos);
        rounds.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        rounds.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle b = new Bundle();
                b.putLong("index", roundList.get(position).getId());
                winners winners = new winners();
                winners.setArguments(b);
                getActivity().getSupportFragmentManager().beginTransaction().hide(rounds.this)
                        .add(R.id.myfrgmentlayout, winners)
                        .addToBackStack("winners")
                        .commit();
            }
        });
        get_rounds();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private void get_rounds() {
        Call<ArrayList<round>> listCall = ApiClient.getApiInterface().getRounds(GlobalKeys.get_lang(getActivity()));

        listCall.enqueue(new Callback<ArrayList<round>>() {
            @Override
            public void onResponse(Call<ArrayList<round>> call, Response<ArrayList<round>> response) {
                loader.setVisibility(View.GONE);
                if (response.body() != null) {
                    roundList = response.body();
                    if(getActivity()!=null){
                        roundAdapter = new RoundAdapterV1(getActivity(),roundList);
                        rounds.setAdapter(roundAdapter);
                    }

                } else {
                    Toast.makeText(getActivity(), R.string.no_rounds, Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<ArrayList<round>> call, Throwable t) {
                loader.setVisibility(View.GONE);
                Toast.makeText(getActivity(), R.string.internet_error, Toast.LENGTH_SHORT).show();

                Log.d("ss", t.toString());
            }
        });
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }

}
