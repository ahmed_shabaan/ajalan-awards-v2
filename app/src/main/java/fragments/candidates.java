package fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Accessors.GlobalKeys;
import Accessors.PrefManager;
import ApiClient.ApiClient;
import adapters.Award_adapter;
import adapters.NewsAdaptor;
import ajlanawards.mivor.com.ajlanawards.R;
import models.*;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link candidates.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link candidates#newInstance} factory method to
 * create an instance of this fragment.
 */
public class candidates extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    List<award> awardList = new ArrayList<>();
    RecyclerView recyclerView;
    Award_adapter award_adapter;
    ProgressBar progressBar;
    long user_id;

    private OnFragmentInteractionListener mListener;

    public candidates() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment candidates.
     */
    // TODO: Rename and change types and number of parameters
    public static candidates newInstance() {
        candidates fragment = new candidates();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_candidates, container, false);
        PrefManager prefManager = new PrefManager(getActivity());
        user_id = prefManager.getUserId();
        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
        main_title.setText(R.string.menue_candidates);
        progressBar = (ProgressBar) view.findViewById(R.id.loader);
        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1, 1, false));
        get_awards(user_id);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void get_awards(long user_id) {
        Call<List<award>> call  = ApiClient.getApiInterface().getAwards(GlobalKeys.get_lang(getActivity()), user_id);

        call.enqueue(new Callback<List<award>>() {
            @Override
            public void onResponse(Call<List<award>> call, Response<List<award>> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    awardList = response.body();
                    award_adapter = new Award_adapter(awardList,getActivity());
                    recyclerView.setAdapter(award_adapter);
                } else {
                    Toast.makeText(getActivity(), R.string.no_awards, Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<award>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.internet_error), Toast.LENGTH_SHORT).show();

                Log.d("ss", t.toString());
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }
}
