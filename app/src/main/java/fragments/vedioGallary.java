package fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Accessors.Farsi;
import Accessors.GlobalKeys;
import Accessors.ImageItem;
import ApiClient.ApiClient;
import adapters.GridViewAdapter;
import adapters.VideoAdapter;
import ajlanawards.mivor.com.ajlanawards.DetailsActivity;
import ajlanawards.mivor.com.ajlanawards.R;
import ajlanawards.mivor.com.ajlanawards.VedioPlayer;
import models.video;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed on 04/12/16.
 */

public class vedioGallary extends Fragment {


    String  Result;
    ArrayList<String> mVedioTitles = new ArrayList<>();
    ArrayList<String> mVedioURLs = new ArrayList<>();

    RecyclerView recyclerView;
    VideoAdapter videoAdapter;
    List<video> videoList;
    ProgressBar progressBar;

    android.app.FragmentManager manager;
    FragmentTransaction transaction;
    android.support.v4.app.Fragment list;

    private gallary.OnFragmentInteractionListener mListener;

    ListView listView ;
    public vedioGallary() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment gallary.
     */
    // TODO: Rename and change types and number of parameters
    public static vedioGallary newInstance() {
        vedioGallary fragment = new vedioGallary();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_vediogallary, container, false);
        long id = getArguments().getLong("index",0);
        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
        main_title.setText(getString(R.string.vedios));
        progressBar = (ProgressBar) view.findViewById(R.id.loader);
        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2, 1, false));
        get_videos(id);

        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof gallary.OnFragmentInteractionListener) {
            mListener = (gallary.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }





    private class HttpRequestTask extends AsyncTask<Void, Void ,Void> {
        @Override
        protected Void doInBackground(Void... params2) {


            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, "UTF-8");
            params.setBooleanParameter("http.protocol.expect-continue", false);
            HttpClient httpclient = new DefaultHttpClient(params);

            HttpGet httpget = new HttpGet(GlobalKeys.GETVEDIOSURL);
            try
            {
                HttpResponse response = httpclient.execute(httpget);
                HttpEntity entity = response.getEntity();
                Result = EntityUtils.toString(entity, HTTP.UTF_8);
                // ...
                JSONArray json = new JSONArray(Result);
// ...

                for(int i=0;i<json.length();i++){
                    JSONObject e = json.getJSONObject(i);


                   mVedioTitles.add(e.getString("title"));
                    mVedioURLs.add(e.getString("video"));

                }
            }
            catch (Exception e)
            {
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, android.R.id.text1, mVedioTitles);
            // Assign adapter to ListView
            listView.setAdapter(adapter);
        }
    }
    private void get_videos(long round_id) {
        Call<List<video>> videoResponseCall = ApiClient.getApiInterface().getVideos(GlobalKeys.get_lang(getActivity()),round_id);

        videoResponseCall.enqueue(new Callback<List<video>>() {
            @Override
            public void onResponse(Call<List<video>> call, Response<List<video>> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    videoList = response.body();
                    videoAdapter = new VideoAdapter(videoList,getActivity());
                    recyclerView.setAdapter(videoAdapter);
                } else {
                    Toast.makeText(getActivity(), R.string.no_videos, Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<video>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(),getString(R.string.internet_error), Toast.LENGTH_SHORT).show();

                Log.d("ss", t.toString());
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }


}
