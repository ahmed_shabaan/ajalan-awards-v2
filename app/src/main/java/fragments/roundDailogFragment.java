package fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import Accessors.RoundClickCallBack;
import adapters.RoundAdapterGallaryList;
import ajlanawards.mivor.com.ajlanawards.R;
import models.round;

/**
 * Created by Ahmed shaban on 12/24/2017.
 */

public class roundDailogFragment extends  android.support.v4.app.DialogFragment implements RoundClickCallBack {
   RecyclerView list;

    List<round> roundList;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rounds_list, container);
        getDialog().setTitle(R.string.selectrount);
        roundList  = getArguments().getParcelableArrayList("list");
        list = (RecyclerView) view.findViewById(R.id.list);
        final RoundAdapterGallaryList roundAdapterGallaryListVideo = new RoundAdapterGallaryList(roundList,getActivity(),this);
        list.setItemAnimator(new DefaultItemAnimator());
        list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        list.setAdapter(roundAdapterGallaryListVideo);

        return view;
    }


    private void notifyToTarget(int code,long id) {
        android.support.v4.app.Fragment targetFragment = getTargetFragment();
        if (targetFragment != null) {
            Intent data  = new Intent();
            data.putExtra("id",id);
            targetFragment.onActivityResult(getTargetRequestCode(), code, data);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void itemClick(long round_id) {
        notifyToTarget(1,round_id);
    }
}
