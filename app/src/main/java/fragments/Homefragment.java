package fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ajlanawards.mivor.com.ajlanawards.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Homefragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Homefragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Homefragment extends Fragment  implements  prizeComunity.OnFragmentInteractionListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ImageView prize1Image , prize2Image , prize3Image , prize4Image ;
    FragmentManager manager;
    FragmentTransaction transaction;
    private OnFragmentInteractionListener mListener;

    public Homefragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Homefragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Homefragment newInstance() {
        Homefragment fragment = new Homefragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view  = inflater.inflate(R.layout.fragment_homefragment, container, false);
        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
        //setHasOptionsMenu(true);

        main_title.setText(R.string.home);
        prize1Image = (ImageView) view.findViewById(R.id.prizeimag1);
        prize2Image = (ImageView) view.findViewById(R.id.prizeimag2);
        prize3Image = (ImageView) view.findViewById(R.id.prizeimag3);
        prize4Image = (ImageView) view.findViewById(R.id.prizeimag4);
        manager =getActivity().getSupportFragmentManager();
        transaction = manager.beginTransaction();
        science_awards science_awards = new science_awards();
        Bundle b = new Bundle();
        prize1Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                science_awards science_awards = new science_awards();
                Bundle b = new Bundle();
                b.putInt("index",1);
                science_awards.setArguments(b);
                transaction.replace(R.id.myfrgmentlayout, science_awards, "science_awards");
                transaction.addToBackStack("science_awards");
                transaction.commit();
            }
        });

        prize3Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                science_awards science_awards = new science_awards();
                Bundle b = new Bundle();
                b.putInt("index",4);
                science_awards.setArguments(b);
                transaction.replace(R.id.myfrgmentlayout, science_awards, "science_awards");
                transaction.addToBackStack("science_awards");
                transaction.commit();
            }
        });

        prize2Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                science_awards science_awards = new science_awards();
                Bundle b = new Bundle();
                b.putInt("index",3);
                science_awards.setArguments(b);
                transaction.replace(R.id.myfrgmentlayout, science_awards, "science_awards");
                transaction.addToBackStack("science_awards");
                transaction.commit();
            }
        });



        prize4Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                science_awards science_awards = new science_awards();
                Bundle b = new Bundle();
                b.putInt("index",2);
                science_awards.setArguments(b);
                transaction.replace(R.id.myfrgmentlayout, science_awards, "science_awards");
                transaction.addToBackStack("science_awards");
                transaction.commit();
            }
        });

        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
