package fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Accessors.GlobalKeys;
import Accessors.PrefManager;
import ApiClient.ApiClient;
import adapters.Award_adapter;
import adapters.HistoryAdapter;
import ajlanawards.mivor.com.ajlanawards.R;
import models.RegistrationHistoryResponse;
import models.award;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class registration_history extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ProgressBar progressBar;
    RecyclerView list;

    PrefManager prefManager;
    ArrayList<RegistrationHistoryResponse> registrationHistoryResponses = new ArrayList<>();
    private HistoryAdapter history_adapter;


    public registration_history() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment registration_history.
     */
    // TODO: Rename and change types and number of parameters
    public static registration_history newInstance(String param1, String param2) {
        registration_history fragment = new registration_history();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  =  inflater.inflate(R.layout.fragment_registration_history, container, false);
        progressBar = view.findViewById(R.id.progressBar);
        list =  view.findViewById(R.id.list);
        prefManager = new PrefManager(getActivity());
        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
        main_title.setText(R.string.menue_registration_history);
        LinearLayoutManager linearLayoutManager  = new LinearLayoutManager(getActivity());
        list.setHasFixedSize(true);
        list.setLayoutManager(linearLayoutManager);
        /*DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(list.getContext(),
                linearLayoutManager.getOrientation());
        list.addItemDecoration(dividerItemDecoration);*/

        history_adapter = new HistoryAdapter(registrationHistoryResponses,getActivity());
        list.setAdapter(history_adapter);
        get_registration_history();

        return  view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void get_registration_history() {
        String base64EncodedCredentials = "Basic " + Base64.encodeToString(
                (prefManager.getUsername() + ":" + prefManager.getPassword()).getBytes(),
                Base64.NO_WRAP);
        Call<ArrayList<RegistrationHistoryResponse>> call  = ApiClient.getApiInterface().gethistory(GlobalKeys.get_lang(getActivity()),"application/json", base64EncodedCredentials);

        call.enqueue(new Callback<ArrayList<RegistrationHistoryResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<RegistrationHistoryResponse>> call, Response<ArrayList<RegistrationHistoryResponse>> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    registrationHistoryResponses.addAll(response.body());
                    history_adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getActivity(), R.string.no_history, Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<ArrayList<RegistrationHistoryResponse>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }

}
