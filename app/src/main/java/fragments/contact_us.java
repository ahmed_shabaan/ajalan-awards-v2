package fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Accessors.GlobalKeys;
import Accessors.utils;
import adapters.SpinAdapter;
import ajlanawards.mivor.com.ajlanawards.R;
import models.SpinnerModel;
import okhttp3.internal.Util;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link contact_us.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link contact_us#newInstance} factory method to
 * create an instance of this fragment.
 */
public class contact_us extends android.support.v4.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private OnFragmentInteractionListener mListener;

    Button send;
    EditText name;
    EditText email;
    EditText subject;
    EditText content;
    ImageView facebook,twitter,google,youtube,insta;
    Intent intentView =  null;
    Spinner message_purpose;
    List<SpinnerModel> spinnerModelList;
    SpinAdapter spinAdapter;
    String message_value;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment contact_us.
     */
    // TODO: Rename and change types and number of parameters
    public static contact_us newInstance(String param1, String param2) {
        contact_us fragment = new contact_us();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public contact_us() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_contact_us, container, false);
        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
        main_title.setText(getString(R.string.menue_contactUs));
         send = (Button) view.findViewById(R.id.send);
        message_purpose = (Spinner) view.findViewById(R.id.message_purpose);
         name  = (EditText) view.findViewById(R.id.name);
         email= (EditText) view.findViewById(R.id.email);
         subject= (EditText) view.findViewById(R.id.subject);
         content= (EditText) view.findViewById(R.id.content);
        facebook = (ImageView) view.findViewById(R.id.facebook);
        google = (ImageView) view.findViewById(R.id.google);
        twitter = (ImageView) view.findViewById(R.id.twitter);
        youtube = (ImageView) view.findViewById(R.id.youtube);
        insta = (ImageView) view.findViewById(R.id.insta);


        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentView = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/ajlanbrosaward/"));
                startActivity(intentView);
            }
        });
        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentView = new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/101592298461904459553/posts"));
                startActivity(intentView);
            }
        });
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentView = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/ajlanbrosaward"));
                startActivity(intentView);
            }
        });
        youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentView = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCBa8nDEqUuTb5_twtrQRk5w"));
                startActivity(intentView);
            }
        });
        insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentView = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/ajlanbrosaward/"));
                startActivity(intentView);
            }
        });

        SpinnerModel spinnerModel = new SpinnerModel();
        spinnerModelList = new ArrayList<>();

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(82);
        spinnerModel.setValue(getString(R.string.enquiry));
        spinnerModelList.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(83);
        spinnerModel.setValue(getString(R.string.complaint));
        spinnerModelList.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(84);
        spinnerModel.setValue(getString(R.string.participation));
        spinnerModelList.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setKey(85);
        spinnerModel.setValue(getString(R.string.other));
        spinnerModelList.add(spinnerModel);

        message_purpose = (Spinner) view.findViewById(R.id.message_purpose);
        spinAdapter = new SpinAdapter(getActivity(),
                android.R.layout.simple_spinner_item, spinnerModelList);
        spinAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        message_purpose.setAdapter(spinAdapter);
        message_purpose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                message_value = spinnerModelList.get(position).getValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        send.setTypeface(GlobalKeys.getFont(getActivity()));


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name.setError(null);
                email.setError(null);
                subject.setError(null);
                content.setError(null);

                String _name =  name.getText().toString().trim();
                String _email = email.getText().toString().trim();
                String _subject = subject.getText().toString().trim();
                String _content = content.getText().toString().trim();

                if(TextUtils.isEmpty(_name)){
                    name.setError(getString(R.string.error_field_required));
                }else  if(TextUtils.isEmpty(_email) ){
                    email.setError(getString(R.string.error_field_required));
                }else  if(!utils.isEmailValid(_email)){
                    email.setError(getString(R.string.error_invalid_email));
                }else  if(TextUtils.isEmpty(_subject)){
                    subject.setError(getString(R.string.error_field_required));
                }else  if(TextUtils.isEmpty(_content)){
                    content.setError(getString(R.string.error_field_required));
                }else{
                    sendEmail(_subject,_name+"-\n"+_content);

                }



            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    protected void sendEmail(String subject,String message) {
        Log.i("Send email", "");
        String[] TO = {"Award@ajlanbros.com"};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject+"-"+message_value);
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);

        try {
            startActivityForResult(Intent.createChooser(emailIntent, "Send mail..."), 1000);

            Log.i(" sending email...", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), R.string.attach_email, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1000 ){
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStack();
        }
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }
}
