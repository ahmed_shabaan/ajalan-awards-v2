package fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adapters.prizeinfoAdapter;
import ajlanawards.mivor.com.ajlanawards.R;
import models.comunity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link prizeComunity.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link prizeComunity#newInstance} factory method to
 * create an instance of this fragment.
 */
public class prizeComunity extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ListView memberList ;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public prizeComunity() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment prizeComunity.
     */
    // TODO: Rename and change types and number of parameters
    public static prizeComunity newInstance() {
        prizeComunity fragment = new prizeComunity();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  =  inflater.inflate(R.layout.fragment_prize_comunity, container, false);

          memberList = (ListView) view.findViewById(R.id.listmember);
        int index = getArguments().getInt("index");
        TextView title = (TextView) view.findViewById(R.id.prize_title);
        TextView main_title = (TextView)((AppCompatActivity) getActivity()).findViewById(R.id.main_title);
        main_title.setText("لجان الجائزة");


        if(index==2){
            title.setText(getString(R.string.idea_content_2));
        }else if(index==3){
            title.setText(getString(R.string.idea_content_3));
        }
        else if(index ==4){
            title.setText(getString(R.string.idea_content_4));
        }else if(index==5){
            title.setText(getString(R.string.commitee_title));
        }


        String[] values = new String[] { "أ/عيد بن عبد العزيز العيد " , "أ/محمد بن سعد العجلان " , "أ/عبد العزيز بن فهد العيد"
        ,"أ/محمد بن صالح العجلان"};
        List<comunity> comunities = new ArrayList<>();
        int type = 1 ;
        if(index==1){
            type = 1;


            comunities.add(new comunity("الأستاذ عيد بن عبد العزيز بن علي العيد .",R.drawable.eed_aziz));
            comunities.add(new comunity("الأستاذ عيد بن عبد العزيز بن علي العيد .",R.drawable.eed_aziz));

            comunities.add(new comunity("الأستاذ محمد بن سعد بن عبدالله العجلان .",R.drawable.mhm_s3d));
            comunities.add(new comunity("الأستاذ عبد العزيز بن فهد بن سعد العيد.",R.drawable.aziz_fhd));
            comunities.add(new comunity("الأستاذ محمد بن صالح بن علي العجلان.",R.drawable.mhm_salh));
            comunities.add(new comunity(" الدكتور منصور بن عبدالعزيز بن محمد العيد.",R.drawable.mnsr_aziz));
            comunities.add(new comunity("الأستاذ علي بن صالح بن محمد العجلان.",R.drawable.aly_0));

        }else if(index==2){
            type = 2;


            comunities.add(new comunity("الأستاذ عيد بن عبد العزيز بن علي العيد .",R.drawable.eed_aziz));
            comunities.add(new comunity("الأستاذ عيد بن عبد العزيز بن علي العيد .",R.drawable.eed_aziz));

            comunities.add(new comunity("الأستاذ محمد بن سعد بن عبدالله العجلان .",R.drawable.mhm_s3d));
            comunities.add(new comunity("الأستاذ عبد العزيز بن فهد بن سعد العيد.",R.drawable.aziz_fhd));
            comunities.add(new comunity("الأستاذ محمد بن صالح بن علي العجلان.",R.drawable.mhm_salh));
            comunities.add(new comunity(" الدكتور منصور بن عبدالعزيز بن محمد العيد.",R.drawable.mnsr_aziz));
            comunities.add(new comunity("الأستاذ علي بن صالح بن محمد العجلان.",R.drawable.aly_0));

        }else if(index==3){
            type = 3;

            comunities.add(new comunity(" الأستاذ ابراهيم عبد العزيز بن صالح العجلان.",R.drawable.ibra_azia));
            comunities.add(new comunity(" الأستاذ ابراهيم عبد العزيز بن صالح العجلان.",R.drawable.ibra_azia));
            comunities.add(new comunity("الأستاذ إبراهيم بن سليمان بن إبراهيم العيد.",R.drawable.ibra_sliman));
            comunities.add(new comunity("الدكتور إبراهيم بن صالح بن عبد العزيز العجلان.",R.drawable.ibra_saleh));
            comunities.add(new comunity("الأستاذ فهد بن سعد بن فهد العجلان.",R.drawable.fhd_s3d));

        }else if(index==4){
            type = 4;


            comunities.add(new comunity("الأستاذ عيد بن عبد العزيز بن علي العيد .",R.drawable.eed_aziz));
            comunities.add(new comunity("الأستاذ عيد بن عبد العزيز بن علي العيد .",R.drawable.eed_aziz));

            comunities.add(new comunity("الأستاذ محمد بن سعد بن عبدالله العجلان .",R.drawable.mhm_s3d));
            comunities.add(new comunity("الأستاذ عبد العزيز بن فهد بن سعد العيد.",R.drawable.aziz_fhd));
            comunities.add(new comunity("الأستاذ محمد بن صالح بن علي العجلان.",R.drawable.mhm_salh));
            comunities.add(new comunity(" الدكتور منصور بن عبدالعزيز بن محمد العيد.",R.drawable.mnsr_aziz));
            comunities.add(new comunity("الأستاذ علي بن صالح بن محمد العجلان.",R.drawable.aly_0));

        }else if(index==5){
            comunities.add(new comunity(" الأستاذ ابراهيم عبد العزيز بن صالح العجلان.",R.drawable.ibra_azia));
            comunities.add(new comunity(" الأستاذ ابراهيم عبد العزيز بن صالح العجلان.",R.drawable.ibra_azia));
            comunities.add(new comunity("الأستاذ إبراهيم بن سليمان بن إبراهيم العيد.",R.drawable.ibra_sliman));
            comunities.add(new comunity("الدكتور إبراهيم بن صالح بن عبد العزيز العجلان.",R.drawable.ibra_saleh));
            comunities.add(new comunity("الأستاذ فهد بن سعد بن فهد العجلان.",R.drawable.fhd_s3d));

            comunities.add(new comunity("الأستاذ عيد بن عبد العزيز بن علي العيد .",R.drawable.eed_aziz));
            comunities.add(new comunity("الأستاذ عيد بن عبد العزيز بن علي العيد .",R.drawable.eed_aziz));

            comunities.add(new comunity("الأستاذ محمد بن سعد بن عبدالله العجلان .",R.drawable.mhm_s3d));
            comunities.add(new comunity("الأستاذ عبد العزيز بن فهد بن سعد العيد.",R.drawable.aziz_fhd));
            comunities.add(new comunity("الأستاذ محمد بن صالح بن علي العجلان.",R.drawable.mhm_salh));
            comunities.add(new comunity(" الدكتور منصور بن عبدالعزيز بن محمد العيد.",R.drawable.mnsr_aziz));
            comunities.add(new comunity("الأستاذ علي بن صالح بن محمد العجلان.",R.drawable.aly_0));
            type = 5 ;

        }



        prizeinfoAdapter adapter  = new prizeinfoAdapter(getActivity(),comunities,type,index);

        memberList.setAdapter(adapter);

        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }
}
