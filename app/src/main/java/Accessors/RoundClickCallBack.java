package Accessors;

/**
 * Created by Ahmed shaban on 12/24/2017.
 */

public interface  RoundClickCallBack {
    void itemClick(long round_id);
}
