package Accessors;

/**
 * Created by Yehia Fathi on 9/26/2016.
 */
import android.content.Context;
import android.content.SharedPreferences;


public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    String username="username";
    String password ="password";

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "prefname";
    private static final String USER_ID = "ID";
    private static final String LOGGED_IN = "logged_in";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public boolean check_login(){
        return  pref.getBoolean(LOGGED_IN,false);
    }

    public  void setLogin(){
        editor.putBoolean(LOGGED_IN,true);
        editor.commit();
    }

    public void logout() {
        editor.putBoolean(LOGGED_IN,false);
        editor.commit();
    }
    public void saveUserID(long user_id) {
        editor.putLong(USER_ID, user_id);
        editor.commit();
    }

    public void set_username_password(String username,String password) {
        editor.putString(this.username, username);
        editor.putString(this.password, password);
        editor.commit();
    }

    public String getUsername(){
        return  pref.getString(username, null);
    }

    public long getUserId(){
        return  pref.getLong(USER_ID, 0);
    }

    public String getPassword(){
        return  pref.getString(password, null);
    }

    public  void  update_language(String lang){
        editor.putString("lang",lang);
        editor.commit();

    }
    public  String  get_lang(){
        return  pref.getString("lang", "ar");

    }



}
