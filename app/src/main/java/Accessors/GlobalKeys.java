package Accessors;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

import ajlanawards.mivor.com.ajlanawards.Login;
import ajlanawards.mivor.com.ajlanawards.R;

/**
 * Created by ahmed on 30/11/16.
 */

public class GlobalKeys {

    public static String GLBURL  =   "http://ajlanbrosaward.com/ajlanbrosaward/";
    public static String IMAGE_URL  =   "http://ajlanbrosaward.com";
    public static String REGESTARTINURL = GLBURL+"test/user/register.json";
    public static String LOGINARTINURL = GLBURL+"test/user/login";
    public static String GETVEDIOSURL = GLBURL+"test/views/videos_api.json";
    public static String GETPHOTOURL = GLBURL+"test/views/photos_by_round_api.json";
    public final  static  String VIDEOURL = "{path}/test/views/videos_by_round_api.json";
    public final  static  String NEWSURL = "{path}/test/views/news_by_round_api.json";
    public final  static  String AROUNDURL = "{path}/test/views/award_round_api.json";
    public final  static  String AWARDS = "{path}/test/views/user_award_reg_status.json";
    public static final String FORGETPASSWORD  = "test/user/request_new_password.json";
    public static final String ARTICLES  = "{path}/test/views/articles_by_round_api.json";
    public static final String LOGS  = "/test/views/testimonials_api.json";
    public static final String VERSIONS = "{path}/test/views/award_issue_api.json";
    public static final String WINNERS = "{path}/test/views/round_winners_api.json";


    public static int AJALANFORSECININCE = 1;
    public static int SAD = 2;
    public static int ABDELAZEZ = 3;
    public static int DOHA = 4;


    public static final int news = 946;
    public static final int articles = 718;
    public static final int images = 347;
    public static final int vedio = 737;

    //some from bitbacket v 2





    public final  static  String get_user_by_id = "test/user/";
    public final  static  int LOGIN_AND_REGISTER_REQUEST = 1000;

    public static  final  int REQUEST_CAMERA= 1000;
    public static final  int SELECT_FILE = 1001;
    public static  String lang="ar";

    public  static  final String youtube_key = "AIzaSyD00lJbTopxeo4MQJVw5nE2kggqidiUmGc";


    public  static void logout(Context context){
        Intent intent = new Intent(context, Login.class);
        PrefManager prefManager = new PrefManager(context);
        prefManager.logout();
        intent.putExtra("finish", true); // if you are checking for this in your other Activities
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    public  static Typeface getFont(Context context){
        String fontPath = "fonts/font.TTF";
        return Typeface.createFromAsset(context.getAssets(), fontPath);
    }




    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);

        return resizedBitmap;
    }
    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static  String  ImageResult(int requestCode , Intent data, ImageView profile,Context  context){
        String Image_bitmap_64 = null;
        if (requestCode == REQUEST_CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");
            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            profile.setImageBitmap(thumbnail);
            Image_bitmap_64 = encodeToBase64(thumbnail,Bitmap.CompressFormat.JPEG,100);
        } else if (requestCode == SELECT_FILE) {
            Uri selectedImageUri = data.getData();
            String[] projection = {MediaStore.MediaColumns.DATA};
            CursorLoader cursorLoader = new CursorLoader(context, selectedImageUri, projection, null, null,
                    null);
            Cursor cursor = cursorLoader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();
            String selectedImagePath = cursor.getString(column_index);
            Bitmap bm;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(selectedImagePath, options);
            final int REQUIRED_SIZE = 200;
            int scale = 1;
            while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                    && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeFile(selectedImagePath, options);
            profile.setImageBitmap(bm);
            Image_bitmap_64 = encodeToBase64(bm,Bitmap.CompressFormat.JPEG,100);
        }

        return  Image_bitmap_64;
    }
    public  static void selectImage(final Context context) {
        final CharSequence[] items = { context.getString(R.string.Take_Photo), context.getString(R.string.gallay_select)};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals( context.getString(R.string.Take_Photo))) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    ((Activity)context).startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals(context.getString(R.string.gallay_select))) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    ((Activity)context).startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    public  static void change_language(final Context context) {
        final CharSequence[] items = {  "العربية", "English" };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle("Select language");
        final PrefManager prefManager = new PrefManager(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("العربية")) {
                    prefManager.update_language("ar");
                    Intent i =  ((Activity) context).getIntent();
                    ((Activity) context).finish();
                    setLocale("ar",context);
                    context.startActivity(i);



                } else if (items[item].equals("English")) {
                    prefManager.update_language("en");
                    Intent i =  ((Activity) context).getIntent();
                    ((Activity) context).finish();
                    setLocale("en", context);
                    context.startActivity(i);

                }
            }
        });
        builder.show();
    }

    public static void setLocale(String lang, Context c) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        c.getApplicationContext().getResources().updateConfiguration(config, null);

    }
    public  static String  get_lang(Context c){
        PrefManager prefManager = new PrefManager(c);
        return prefManager.get_lang();

    }



}
