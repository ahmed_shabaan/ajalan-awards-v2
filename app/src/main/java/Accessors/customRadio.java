package Accessors;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

/**
 * Created by ahmed shabaan on 12/26/2016.
 */
public class customRadio extends RadioButton {

    public customRadio(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public customRadio(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public customRadio(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/font.TTF");
            setTypeface(tf);
        }
    }

}
