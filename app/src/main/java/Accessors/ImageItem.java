package Accessors;

import android.graphics.Bitmap;

/**
 * Created by ahmed on 24/11/16.
 */

public class ImageItem {
    private Bitmap image;
    private String title;
    private String fullURL;
    private String thumbURL;

    public ImageItem() {
        super();
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFullURL() {
        return fullURL;
    }

    public void setFullURL(String fullURL) {
        this.fullURL = fullURL;
    }

    public String getThumbURL() {
        return thumbURL;
    }

    public void setThumbURL(String thumbURL) {
        this.thumbURL = thumbURL;
    }
}
